package lib

import (
	"github.com/veritrans/go-midtrans"
	"strconv"
	"time"
)


var midclient midtrans.Client
var coreGateway midtrans.CoreGateway
var snapGateway midtrans.SnapGateway

func SetupMidtrans() {
	midclient = midtrans.NewClient()
	midclient.ServerKey = "SB-Mid-server-2a1HmrgsJHHcxa3_tRLZ2yN0"
	midclient.ClientKey = "SB-Mid-client-yfkLmXC6miEmGrma"
	midclient.APIEnvType = midtrans.Sandbox

	coreGateway = midtrans.CoreGateway{
		Client: midclient,
	}

	snapGateway = midtrans.SnapGateway{
		Client: midclient,
	}
}


func GenerateOrderID() string {
	return strconv.FormatInt(time.Now().UnixNano(), 10)
}
