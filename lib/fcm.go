package lib

import (
	"github.com/bandros/framework"
)

const (
	FCM_URL = "https://fcm.googleapis.com/fcm/send"
	FCM_KEY = "AIzaSyAp6aLvaph0s2oAWMgz-h7-1HWsq2x5vZ4"
)

type SendFCM struct {
	To           string `json:"to"`
	time         int    `json:"time_to_live"`
	priority     string `json:"priority"`
	pkgAndroid   string `json:"restricted_package_name"`
	Notification struct {
		Title           string `json:"title"`
		Text            string `json:"body"`
		priority        string `json:"priority"`
		androidChanelId string `json:"android_chanel_id"`
	} `json:"notification"`
	Data struct {
		IDOrder string `json:"id"`
	} `json:"data"`
}

func (data *SendFCM) Send() error {
	var api = framework.Api{}
	data.time = 2419200
	data.priority = "high"
	data.pkgAndroid = "com.example.wahwah"
	data.Notification.priority = "high"
	data.Notification.androidChanelId = "com.example.wahwah.notification.channel"
	var err = api.JsonData(data)
	if err != nil {
		return err
	}
	api.Header = map[string]string{"Authorization": "key=" + FCM_KEY}
	api.ContentType = "application/json"
	api.Url = FCM_URL
	err = api.Do("POST")
	if err != nil {
		return err
	}
	return nil
}
