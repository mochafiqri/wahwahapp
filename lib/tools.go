package lib

import (
	"fmt"
	"github.com/bandros/framework"
	"github.com/disintegration/imaging"
	"image"
	"log"
	"math/rand"
	"mime/multipart"
	"regexp"
	"strings"
	"wahwah/model"
)

func bucket() string {
	var state = framework.Config("state")
	if state == "beta" {
		return "s1.tss.my.id"
	} else if state == "localhost" {
		return "localhost:8080"
	} else {
		return "s4.bandros.co.id"
	}
}

//Slug is lib for create slug
func Slug(str string) string {
	reg, _ := regexp.Compile("[^a-zA-Z0-9]")
	var slug = reg.ReplaceAllString(str, "-")
	reg, _ = regexp.Compile("[-]+")
	slug = reg.ReplaceAllString(slug, "-")
	slug = strings.Trim(slug, "-")
	return strings.ToLower(slug)
}

func KodeUnik() int{
	var sukses bool = true
	var kode_unik int
	for sukses {
		kode_unik = rand.Intn(999)
		tmp,err := model.CekKodeUnik(kode_unik)
		if err != nil {
			fmt.Println(err.Error())
		}
		if tmp == nil {
			sukses = false
		}
	}
	return kode_unik
}

func Resize (file *multipart.FileHeader, width,height int,filepath string) error {
	f, err := file.Open()
	if err != nil {
		return err
	}
	defer f.Close()
	src, err := imaging.Decode(f)
	if err != nil {
		return err
	}
	 widthraw, heightraw := getSize(src.Bounds())

	if (width > 0 || height > 0) && width < widthraw && height < heightraw {
		src = imaging.Resize(src, width, height, imaging.Lanczos)
	}
	// Save the resulting image as JPEG.
	err = imaging.Save(src, filepath)
	if err != nil {
		log.Fatalf("failed to save image: %v", err)
	}
	return nil
}

func getSize(img image.Rectangle) (int, int) {
	b := img.Bounds()
	width := b.Max.X
	height := b.Max.Y
	return width, height
}