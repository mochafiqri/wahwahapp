package main

import (
	"github.com/bandros/framework"
	"wahwah/lib"
	"wahwah/router"
)

func main(){
	fw := framework.Init{}
	fw.Get()
	r := fw.Begin
	router.Init(r)
	lib.SetupMidtrans()
	fw.Run()
}