var table = $('#table-review').DataTable( {
    "responsive": true,
    "serverSide": true,
    "ajax": {
        "url": "/api/admin/review/list",
        "type": "POST",
        "data" : {
            "id_event" : $("#id_event").val()
        }
    },
    columns: [
        { data: "id" },
        { data: "nama" },
        { data: "rating" },
        { data: "review" },
      ]
} );

