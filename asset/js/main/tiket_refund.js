$(document).ready(function() {
    $(function () {
        //Exportable table
        $('.js-exportable').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    });
    //
    //
    var tableRefund = $('#table-refund').DataTable( {
        "responsive": true,
        "serverSide": true,
        "ajax": {
            "url": "/api/admin/refund/list",
            "type": "POST"
        },
        columns: [
            { data: "id" },
            { data: "tgl_request" },
            { data: "nama_user" },
            { data: "status",
                render: function (data, type, row, meta) {
                    if (data == "0"){
                        return "Belum Konfirmasi"
                    }else if (data == "1"){
                        return "Berhasil Refund"
                    }else if (data == "2"){
                        return "Cancel Refund"
                    }
                }},
            { data: "bank" },
            { data: "no_rek" },
            { "defaultContent": "<div class='icon-button-demo '>" +
                    "<button title='Detail' class='btn btn-success btn-circle waves-effect waves-circle waves-float  btn-detail'>  <i class=\"material-icons\">details</i></button>"+
                    "<button title='Check' class='btn btn-danger btn-circle waves-effect waves-circle waves-float btn-check' >  <i class=\"material-icons\">check</i></button>"+
                    "<button title='Cancel' class='btn btn-primary btn-circle waves-effect waves-circle waves-float btn-cancel' >  <i class=\"material-icons\">cancel</i></button>"+
                    "</div>"}
        ],
    } );

    var table = $('#table-pending').DataTable( {
        "responsive": true,
        "serverSide": true,
        "ajax": {
            "url": "/api/admin/request-refund/list",
            "type": "POST"
        },
        columns: [
            { data: "id" },
            { data: "tgl_request" },
            { data: "nama_user" },
            { data: "status",
                render: function (data, type, row, meta) {
                    if (data == "0"){
                        return "Belum Konfirmasi"
                    }else if (data == "1"){
                        return "Berhasil Refund"
                    }else if (data == "2"){
                        return "Cancel Refund"
                    }
                }},
            { data: "bank" },
            { data: "no_rek" },
            { "defaultContent": "<div class='icon-button-demo '>" +
                    "<button title='Detail' class='btn btn-success btn-circle waves-effect waves-circle waves-float  btn-detail'>  <i class=\"material-icons\">details</i></button>"+
                    "<button title='Check' class='btn btn-danger btn-circle waves-effect waves-circle waves-float btn-check' >  <i class=\"material-icons\">check</i></button>"+
                    "<button title='Cancel' class='btn btn-primary btn-circle waves-effect waves-circle waves-float btn-cancel' >  <i class=\"material-icons\">cancel</i></button>"+
                    "</div>"}
        ],
    } );

    $('table tbody').on( 'click', '.btn-check', function (e) {
        var id = table.row($(this).parents('tr')).data().id;
        $.ajax({
            url : "api/admin/order/confirm",
            data : {id_order:id},
            type : "POST",
            success : function (r) {
                table.ajax.reload();
                showNotification("alert-success",r.msg,
                    "bottom","center","","",);
                table.ajax.reload();
            },
            error : function (r) {
                $('.page-loader-wrapper').fadeOut();
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
            }

        });
    });

    $('table tbody').on( 'click', '.btn-cancel', function (e) {
        var id = table.row($(this).parents('tr')).data().id;
        $.ajax({
            url : "api/admin/order/cancel",
            data : {id_order:id},
            type : "POST",
            success : function (r) {
                table.ajax.reload();
                showNotification("alert-success",r.msg,
                    "bottom","center","","",);
            },
            error : function (r) {
                $('.page-loader-wrapper').fadeOut();
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
            }

        });
    });

    $('table tbody').on( 'click', '.btn-check', function (e) {
        var id = table.row($(this).parents('tr')).data().id;
        $.ajax({
            url : "api/admin/request-refund/konfirm",
            data : {id_refund:id},
            type : "POST",
            success : function (r) {
                table.ajax.reload();
                showNotification("alert-success",r.msg,
                    "bottom","center","","",);
            },
            error : function (r) {
                $('.page-loader-wrapper').fadeOut();
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
            }

        });
    });
    $('table tbody').on( 'click', '.btn-cancel', function (e) {
        var id = table.row($(this).parents('tr')).data().id;
        $.ajax({
            url : "api/admin/request-refund/cancel",
            data : {id_refund:id},
            type : "POST",
            success : function (r) {
                table.ajax.reload();
                showNotification("alert-success",r.msg,
                    "bottom","center","","",);
            },
            error : function (r) {
                $('.page-loader-wrapper').fadeOut();
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
            }

        });
    });

});