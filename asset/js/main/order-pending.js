$(document).ready(function() {
    $(function () {
        //Exportable table
        $('.js-exportable').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    });
    //
    //
    var table = $('#table-pending').DataTable( {
        "responsive": true,
        "serverSide": true,
        "ajax": {
            "url": "/api/admin/order/pending",
            "type": "POST"
        },
        columns: [
            { data: "id" },
            { data: "tanggal" },
            { data: "nama_user" },
            { data: "status",
                render: function (data, type, row, meta) {
                    if (data == "0"){
                        return "Belum Pilih Tipe Pembayaran"
                    }else if (data == "1"){
                        return "Belum Konfirmasi"
                    }else if (data == "2"){
                        return "Sudah Konfirmasi"
                    }else if (data == "3"){
                        return "Order Selesai"
                    }else if (data == "4"){
                        return "Cancel Order"
                    }
                }},
            { data: "total" },
            { data: "qty" },
            {
                data: "bukti_foto",
                render: function (data, type, row, meta) {
                    if (data != "") {
                        return "<img class='img-bukti' style='width: 100px' src=" + data + ">"
                    }else{
                        return ""
                    }
                }
            },

            { "defaultContent": "<div class='icon-button-demo '>" +
                    "<button title='Detail' class='btn btn-success btn-circle waves-effect waves-circle waves-float  btn-detail'>  <i class=\"material-icons\">details</i></button>"+
                    "<button title='Check' class='btn btn-primary btn-circle waves-effect waves-circle waves-float btn-check' >  <i class=\"material-icons\">check</i></button>"+
                    "<button title='Cancel' class='btn btn-danger btn-circle waves-effect waves-circle waves-float btn-cancel' >  <i class=\"material-icons\">cancel</i></button>"+
                    "</div>"}
            ],
    } );

    $('table tbody').on( 'click', '.btn-check', function (e) {
        var id = table.row($(this).parents('tr')).data().id;
            $.ajax({
                url : "api/admin/order/confirm",
                data : {id_order:id},
                type : "POST",
                success : function (r) {
                    table.ajax.reload();
                    showNotification("alert-success",r.msg,
                        "bottom","center","","",);
                    table.ajax.reload();
                },
                error : function (r) {
                    $('.page-loader-wrapper').fadeOut();
                    showNotification("alert-danger",r.msg,
                        "bottom","center","","",);
                }

            });
    });

    $('table tbody').on( 'click', '.btn-cancel', function (e) {
        var id = table.row($(this).parents('tr')).data().id;
        $.ajax({
            url : "api/admin/order/cancel",
            data : {id_order:id},
            type : "POST",
            success : function (r) {
                table.ajax.reload();
                showNotification("alert-success",r.msg,
                    "bottom","center","","",);
                table.ajax.reload();
            },
            error : function (r) {
                $('.page-loader-wrapper').fadeOut();
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
            }

        });
    });

    $('table tbody').on( 'click', '.img-bukti', function (e) {
        var bukti = table.row($(this).parents('tr')).data().bukti_foto;
        window.open(bukti)
    });

    // $('table tbody').on( 'click', '.btn-publish', function (e) {
    //     var id = table.row($(this).parents('tr')).data().id;
    //     $.ajax({
    //         url : "api/admin/band/publish",
    //         data : {id:id},
    //         type : "POST",
    //         success : function (r) {
    //             table.ajax.reload();
    //             showNotification("alert-success",r.msg,
    //                 "bottom","center","","",);
    //         },
    //         error : function (r) {
    //             $('.page-loader-wrapper').fadeOut();
    //             showNotification("alert-danger",r.msg,
    //                 "bottom","center","","",);
    //         }
    //
    //     });
    // });

});