// initDonutChart1(75,25);
// initDonutChart2(75,25);
$(".sentimen-1").hide();
$(".sentimen-2").hide();
$(".web-search").hide();
$(".youtube-search").hide();

if ($("#keyword-1").val() != "" && $("#keyword-2").val() != ""){
    Proses();
}



$("#search").click(function (e) {
    var keyword1 = $("#keyword-select-1").val();
    var keyword2 = $("#keyword-select-2").val();
    if (keyword1 == "" || keyword2 == "") {
        showNotification("alert-info","Keyword tidak boleh kosong",
            "bottom","center","","",);
        return
    }
    window.location = '/sentimen?sentimen1='+ keyword1+'&sentimen2='+keyword2;
});



function initDonutChart1(v1,v2) {
    $(".sentimen-1").show();
    Morris.Donut({
        element: 'donut_chart-1',
        data: [{
            label: 'Positive',
            value: v1
        }, {
            label: 'Negative',
            value: v2
        },
          ],
        colors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(255, 152, 0)', 'rgb(0, 150, 136)', 'rgb(96, 125, 139)'],
        formatter: function (y) {
            return y + '%'
        }
    });
}function initDonutChart2(v1,v2) {
    $(".sentimen-2").show();
    Morris.Donut({
        element: 'donut_chart-2',
        data: [{
            label: 'Positive',
            value: v1
        }, {
            label: 'Negative',
            value: v2
        },
          ],
        colors: ['rgb(233, 30, 99)', 'rgb(0, 188, 212)', 'rgb(255, 152, 0)', 'rgb(0, 150, 136)', 'rgb(96, 125, 139)'],
        formatter: function (y) {
            return y + '%'
        }
    });
}

function Proses(){
    $('.page-loader-wrapper').fadeIn();
    $(".web-search").show();
    $(".youtube-search").show();
    var keyword = [$("#keyword-1").val(),$("#keyword-2").val()];
    console.log(keyword);
    showNotification("alert-info","Harap Tunggu, Analisis Sentimen Membutuhkan Waktu ...",
        "bottom","center","","",);
    $.ajax({
        url : "api/admin/sentimen/get",
        data : {keyword:keyword},
        type : "POST",
        success : function (r) {
            $('.page-loader-wrapper').fadeOut();
            console.log(r.data);
            showNotification("alert-success",r.msg,
                "bottom","center","","",);
            initDonutChart1(r.data.sentimen_positive1,r.data.sentimen_negative1);
            initDonutChart2(r.data.sentimen_positive2,r.data.sentimen_negative2);
        },
        error : function (r) {
            $('.page-loader-wrapper').fadeOut();
            showNotification("alert-danger",r.msg,
                "bottom","center","","",);
        }

    });

}