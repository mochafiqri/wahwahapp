var table = $('#table-keuangan').DataTable( {
    "responsive": true,
    "serverSide": true,
    "ajax": {
        "url": "/api/admin/catatan-keuangan/get-dtb",
        "type": "POST",
        "data" : {
            "id_event" : $("#id_event").val()
        }
    },
    columns: [
        { data: "id" },
        { data: "tanggal" },
        { data: "kategori" },
        { data: "kredit" },
        { data: "debit" },
        { data: "saldo" },
        { data: "ket" },
        { "defaultContent": "<div class='icon-button-demo '>" +
                "<button title='Detail' class='btn btn-success btn-circle waves-effect waves-circle waves-float  btn-detail'>  <i class=\"material-icons\">details</i></button>"+

                "</div>"}
    ],
} );

$("#formAdd-debit").submit(function (e) {
    e.preventDefault();
    var idEvent = $("#id_event").val();
    var idKategori = $("#kategori_tiket_debit").val()
    var jumlah = $("#jumlah-debit").val()
    var ket = $("#ket-debit").val();

    console.log(idEvent);
    console.log(idKategori);
    console.log(jumlah);
    AddCatatanKeuangan(idEvent,idKategori,0,jumlah,ket);
    $(this).trigger("reset");

});
$("#formAdd-kredit").submit(function (e) {
    e.preventDefault();
    var idEvent = $("#id_event").val();
    var idKategori = $("#kategori_tiket_kredit").val()
    var jumlah = $("#jumlah-kredit").val();
    var ket = $("#ket-kredit").val();

    console.log(idEvent);
    console.log(idKategori);
    console.log(jumlah);
    AddCatatanKeuangan(idEvent,idKategori,jumlah,0,ket);
    $(this).trigger("reset");
});


$("#frmAddKategori-kredit").submit(function (e) {
    e.preventDefault();
    nama = $("#nama_kategori_kredit").val();
    var type = 2;
    console.log(nama)
    AddKategoriKeuangan(nama,type)
});

$("#frmAddKategori-debit").submit(function (e) {
    e.preventDefault();
    var nama = $("#nama_kategori-debit").val();
    var type = 1
    console.log("Debit")

    AddKategoriKeuangan(nama,type)
});


// AddCatatanKeuangan(2,1,100000,0,"Test");
function AddCatatanKeuangan(idEvent,idKategori,kredit,debit,ket) {
    $('.page-loader-wrapper').fadeIn();

    $.ajax({
        url : "/api/admin/catatan-keuangan/add",
        data : {
            id_event : idEvent,
            id_kategori : idKategori,
            kredit : kredit,
            debit : debit,
            keterangan : ket,
        },
        type : "POST",
        success : function (r) {
            $('.page-loader-wrapper').fadeOut();
            table.ajax.reload();
            if (r.code == 200){
                showNotification("alert-success",r.msg,
                    "bottom","center","","",);
            }else{
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);

            }
        },
        error : function (r) {
            $('.page-loader-wrapper').fadeOut();
            showNotification("alert-danger",r.msg,
                "bottom","center","","",);
        }
    });
};

function AddKategoriKeuangan(nama,type) {
    $('.page-loader-wrapper').fadeIn();
    $.ajax({
        url : "/api/admin/catatan-keuangan-kategori/add",
        data : {
            nama : nama,
            type : type,

        },
        type : "POST",
        success : function (r) {
            $('.page-loader-wrapper').fadeOut();
            table.ajax.reload();
            if (r.code == 200){
                showNotification("alert-success",r.msg,
                    "bottom","center","","",);
                location.reload();

            }else{
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);

            }
        },
        error : function (r) {
            $('.page-loader-wrapper').fadeOut();
            showNotification("alert-danger",r.msg,
                "bottom","center","","",);
        }
    });
}