$("#formAddNotif").submit(function (e) {
    e.preventDefault();
    var data = $(this).serialize();

    $.ajax({
        url : "/api/admin/notification/add",
        data : data,
        type : "POST",
        success : function (r) {
            $('.page-loader-wrapper').fadeOut();
            if (r.code == 200){
                showNotification("alert-success",r.msg,
                    "bottom","center","","",);

            }else{
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);

            }
        },
        error : function (r) {
            $('.page-loader-wrapper').fadeOut();
            showNotification("alert-danger",r.msg,
                "bottom","center","","",);
        }
    });

});