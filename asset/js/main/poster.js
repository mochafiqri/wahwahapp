
$("#frmFileUpload").submit(function (e) {
    e.preventDefault();
    return;
    var $frm = $(this);
    var formData = new FormData(this);

    $.ajax({
        type:'POST',
        url: $(this).attr('action'),
        data:formData,
        cache:false,
        contentType: false,
        processData: false,
        success : function (r) {
            showNotification("alert-success",r.msg,
                "bottom","center","","",);
        },
        error : function (r) {
            $('.page-loader-wrapper').fadeOut();
            showNotification("alert-danger",r.msg,
                "bottom","center","","",);
        }
    });
})