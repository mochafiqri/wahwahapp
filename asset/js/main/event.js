$(document).ready(function() {
    //
    //
    var table = $('#table-event').DataTable( {
        "responsive": true,
        "serverSide": true,
        "ajax": {
            "url": "/api/admin/event/datatable",
            "type": "POST"
        },
        columns: [
            { data: "id" },
            { data: "nama" },
            { data: "deskripsi" },
            { data: "status" },
            { data: "publish",
                render: function (data, type, row, meta) {
                    if (data == 1){
                        return "Publish"
                    }else {
                        return "Unpublish"
                    }
                }},

            { "defaultContent": "<div class='icon-button-demo '>" +
                    "<button title='Detail' class='btn btn-success btn-circle waves-effect waves-circle waves-float  btn-edit'>  <i class=\"material-icons\">more_vert</i></button>"+
                    "<button title='Foto' class='btn btn-danger btn-circle waves-effect waves-circle waves-float btn-foto' >  <i class=\"material-icons\">image</i></button>"+
                    "<button title='Publish' class='btn btn-primary btn-circle waves-effect waves-circle waves-float btn-publish' >  <i class=\"material-icons\">publish</i></button>"+

                    "</div>"}
            ],
    } );
    $(".btn-tambah").click(function (e) {
       $(".btn-delete").hide();
        $("#frmAddEvent").trigger("reset");

    });
    $("#frmAddEvent").submit(function (e) {
        e.preventDefault();
        $('.page-loader-wrapper').fadeIn();
        var method = $(this).attr("method");
        var data = $(this).serialize();
        var modal = $("#tambahEventModal");
        id = $("#id").val();
        var url = "/api/admin/event/create";
        if (id != "") {
            url = "/api/admin/event/update";
        }

        $.ajax({
            url : url,
            data : data,
            type : method,
            success : function (r) {
                $('.page-loader-wrapper').fadeOut();
                $(this).trigger("reset");
                modal.modal('hide');

                table.ajax.reload();
                if (r.code == 200){
                    showNotification("alert-success",r.msg,
                        "bottom","center","","",);
                    var goToEvent = "/event/"+r.data.id_band;
                    setTimeout(function () {
                        document.location=goToEvent;
                    },1000);
                }else{
                    showNotification("alert-danger",r.msg,
                        "bottom","center","","",);

                }
            },
            error : function (r) {
                $('.page-loader-wrapper').fadeOut();
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
            }
        });
    });

    $('table tbody').on( 'click', '.btn-edit', function (e) {
        var id = table.row($(this).parents('tr')).data().id;
        // window.location.replace("localhost:8080/event/"+id);
        window.location.replace("event/"+id);
        // $(".title-modal").html("Edit Band");
        // $(".btn-delete").show();
        // $("#frmAddEvent").trigger("reset");
        //
        // $.ajax({
        //     url : "api/admin/event/getById",
        //     data : {id:id},
        //     type : "POST",
        //     success : function (r) {
        //
        //         $("#tambahEventModal").modal("toggle");
        //         $("#id").val(r.data.id);
        //         $("#nama").val(r.data.nama);
        //         $("#deskripsi").val(r.data.deskripsi);
        //     },
        //     error : function (r) {
        //         $('.page-loader-wrapper').fadeOut();
        //         showNotification("alert-danger",r.msg,
        //             "bottom","center","","",);
        //     }
        // });
    });

    $('table tbody').on( 'click', '.btn-foto', function (e) {
        var id = table.row($(this).parents('tr')).data().id;
        // window.location.replace("localhost:8080/event/"+id);
        window.location.replace("/event-poster/"+id);

    });

    $('table tbody').on( 'click', '.btn-publish', function (e) {
        var id = table.row($(this).parents('tr')).data().id;
        $.ajax({
            url : "api/admin/band/publish",
            data : {id:id},
            type : "POST",
            success : function (r) {
                table.ajax.reload();
                showNotification("alert-success",r.msg,
                    "bottom","center","","",);
            },
            error : function (r) {
                $('.page-loader-wrapper').fadeOut();
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
            }

        });
    });
});
