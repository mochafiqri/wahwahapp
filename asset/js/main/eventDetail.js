var venue = $("#tmpVenue").val();
$("#id_tempat").val(venue);
$('.datetimepicker').bootstrapMaterialDatePicker({
    format: 'YYYY-MM-DD HH:mm',
    clearButton: true,
    weekStart: 1
});




$("#formBasicInformation").submit(function (e) {
    e.preventDefault();
    var url = "/api/admin/event/basic-update";
    var id = $("#id_event").val();
 
    var data = $(this).serialize();
    var method = "POST";
    
    $('.page-loader-wrapper').fadeIn();


    $.ajax({
        url : url,
        data : data + '&id=' + id,
        type : method,
        success : function (r) {
            $('.page-loader-wrapper').fadeOut();

            if (r.code == 200){
                showNotification("alert-success",r.msg,
                    "bottom","center","","",);
                $(".band_event").click();
            }else{
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);

            }
        },
        error : function (r) {
            $('.page-loader-wrapper').fadeOut();
            showNotification("alert-danger",r.msg,
                "bottom","center","","",);
        }
    });
});


$("#tambah_kategori").click(function (e) {
    $("#tambahKategoriTiketModal").modal("show");
});

$("#frmAddKategoriTiket").submit(function (e) {
    e.preventDefault();

    id = $("#id_event").val();

    var data = $(this).serialize();

    $.ajax({
        url : '/api/admin/event-kategori/create',
        data : data + "&id_event="+id,
        type : 'POST',
        success : function (r) {
            if (r.code == 200){
                window.location.replace("../event/"+id);
            }else{
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);

            }
        },
        error : function (r) {
            $('.page-loader-wrapper').fadeOut();
            showNotification("alert-danger",r.msg,
                "bottom","center","","",);
        }
    });
});

$("#formTiketAdd").submit(function (e) {
    e.preventDefault();
    id = $("#id_event").val();
    var data = $(this).serialize();
    $('.page-loader-wrapper').fadeIn();
    $.ajax({
        url : '/api/admin/tiket/create',
        data : data + "&id_event="+id,
        type : 'POST',
        success : function (r) {
            $('.page-loader-wrapper').fadeOut();
            if (r.code == 200){
                showNotification("alert-success",r.msg,
                    "bottom","center","","",);
                tableTiketKategori.ajax.reload()
            }else{
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);

            }
        },
        error : function (r) {
            $('.page-loader-wrapper').fadeOut();
            showNotification("alert-danger",r.msg,
                "bottom","center","","",);
        }
    });
});

$("#formPerformer").submit(function (e) {
    e.preventDefault();
    id = $("#id_event").val();
    var data = $(this).serialize();
    $('.page-loader-wrapper').fadeIn();
    $.ajax({
        url : '/api/admin/event/set-performer',
        data : data + "&id_event="+id,
        type : 'POST',
        success : function (r) {
            $('.page-loader-wrapper').fadeOut();
            if (r.code == 200){
                showNotification("alert-success",r.msg,
                    "bottom","center","","",);
                tablePerformer.ajax.reload()
            }else{
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);

            }
        },
        error : function (r) {
            $('.page-loader-wrapper').fadeOut();
            showNotification("alert-danger",r.msg,
                "bottom","center","","",);
        }
    });
});


var tablePerformer = $('#table-event-performer').DataTable( {
    "responsive": true,
    "serverSide": true,
    "ajax": {
        "url": "/api/admin/event/performer-list",
        "type": "POST",
        "data" : {
            "id_event" : $("#id_event").val()
        }
    },
    columns: [
        { data: "id" },
        { data: "nama" },
        { data: "rundown" },
        { "defaultContent": "<div class='icon-button-demo '>" +
                "<button title='Edit' class='btn btn-success btn-circle waves-effect waves-circle waves-float btn-rundown'>  <i class=\"material-icons\">access_time</i></button>"+
                "</div>"}

    ],
} );
var table = $('#table-tiket').DataTable( {
    "responsive": true,
    "serverSide": true,
    "ajax": {
        "url": "/api/admin/tiket/sold",
        "type": "POST",
        "data" : {
            "id_event" : $("#id_event").val()
        }
    },
    columns: [
        { data: "id_tiket" },
        { data: "nama_kategori" },
        { data: "harga" },
        { data: "nama" },
        { data: "status",
            render: function (data, type, row, meta) {
                if (data == 1){
                    return "Belum Checkin"
                }else if (data == 3) {
                    return "Permohonan Refund"
                }else if (data == 2){
                    return "Sudah Checkin"
                }
            }},
    ],
} );
var tableTiketKategori = $('#table-tiket-kategori').DataTable( {
    "responsive": true,
    "serverSide": true,
    "ajax": {
        "url": "/api/admin/tiket-kategori/list",
        "type": "POST",
        "data" : {
            "id_event" : $("#id_event").val()
        }
    },
    columns: [
        { data: "id" },
        { data: "nama" },
        { data: "harga" },
        { data: "sisa_tiket" },
        { data: "status",
            render: function (data, type, row, meta) {
                if (data == 1){
                    return "Aktif"
            } if (data == 2){
                    return "Non Aktif"
                }

            }},        { data: "status",
            render: function (data, type, row, meta) {
                if (data == 1){
                    return "<div class='icon-button-demo '>" +
                        "<button title='Set Non ' class='btn btn-danger btn-circle waves-effect waves-circle waves-float btn-sold'>  <i class=\"material-icons\">close</i></button>"+
                        "</div>"
                } if (data == 2){
                    return "<div class='icon-button-demo '>" +
                        "<button title='Set Non ' class='btn btn-primary btn-circle waves-effect waves-circle waves-float btn-sold'>  <i class=\"material-icons\">check</i></button>"+
                        "</div>"
                }

            }},

    ],
} );

$('#status_refundable').change(function() {
    $('.page-loader-wrapper').fadeIn();
    var idEvent = $("#id_event").val();

    $.ajax({
        url : '/api/admin/event/refundable',
        data : {
            "id_event" : idEvent
        },
        type : 'POST',
        success : function (r) {
            $('.page-loader-wrapper').fadeOut();
            if (r.code == 200){
                showNotification("alert-success",r.msg,
                    "bottom","center","","",);

            }else{
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
            }
        },
        error : function (r) {
            $('.page-loader-wrapper').fadeOut();
            showNotification("alert-danger",r.msg,
                "bottom","center","","",);
        }
    });
});


$('#table-event-performer tbody').on( 'click', '.btn-rundown', function (e) {
    e.preventDefault();
    var id = tablePerformer.row($(this).parents('tr')).data().id;
    $("#Modaledit_rundown").modal("show")
    $("#id_event_band").val(id)
})

$('#table-tiket-kategori tbody').on( 'click', '.btn-sold', function (e) {
    var id = tableTiketKategori.row($(this).parents('tr')).data().id;

    $.ajax({
        url : "/api/admin/tiket-kategori/sold",
        data : {id_tiket_kategori:id},
        type : "POST",
        success : function (r) {
            table.ajax.reload();
            showNotification("alert-success",r.msg,
                "bottom","center","","",);
            tableTiketKategori.ajax.reload()
            $("#Modaledit_rundown").modal("hide")

        },
        error : function (r) {
            $('.page-loader-wrapper').fadeOut();
            showNotification("alert-danger",r.msg,
                "bottom","center","","",);
        }

    });
});

$("#selesaikan-event").click(function (e) {
    e.preventDefault()
    $('.page-loader-wrapper').fadeIn();
    var idEvent = $("#id_event").val();

    $.ajax({
        url : '/api/admin/event/selesaikan',
        data : {
            "id_event" : idEvent
        },
        type : 'POST',
        success : function (r) {
            $('.page-loader-wrapper').fadeOut();
            if (r.code == 200){
                showNotification("alert-success",r.msg,
                    "bottom","center","","",);
                setTimeout(function () {
                    location.reload();
                },2000);
            }else{
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
            }
        },
        error : function (r) {
            $('.page-loader-wrapper').fadeOut();
            showNotification("alert-danger",r.msg,
                "bottom","center","","",);
        }
    });
})


$(".next-step").click(function (e) {
    e.preventDefault()
    status = $(this).data("id");

    $('.page-loader-wrapper').fadeIn();
    var idEvent = $("#id_event").val();

    $.ajax({
        url : '/api/admin/event/set-status',
        data : {
            "id_event" : idEvent,
            "status" : status
        },
        type : 'POST',
        success : function (r) {
            $('.page-loader-wrapper').fadeOut();
            if (r.code == 200){
                showNotification("alert-success",r.msg,
                    "bottom","center","","",);
                setTimeout(function () {
                    location.reload();
                },2000);
            }else{
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
            }
        },
        error : function (r) {
            $('.page-loader-wrapper').fadeOut();
            showNotification("alert-danger",r.msg,
                "bottom","center","","",);
        }
    });
})


$("#frmEditRundown").submit(function (e) {
    e.preventDefault();
    id = $("#id_event").val();
    var data = $(this).serialize();
    $('.page-loader-wrapper').fadeIn();
    $.ajax({
        url : '/api/admin/event/set-rundown',
        data : data,
        type : 'POST',
        success : function (r) {
            $('.page-loader-wrapper').fadeOut();
            if (r.code == 200){
                showNotification("alert-success",r.msg,
                    "bottom","center","","",);
                tablePerformer.ajax.reload()


            }else{
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);

            }
        },
        error : function (r) {
            $('.page-loader-wrapper').fadeOut();
            showNotification("alert-danger",r.msg,
                "bottom","center","","",);
        }
    });
});
