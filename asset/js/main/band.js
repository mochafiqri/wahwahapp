$(document).ready(function() {
    $(function () {
        //Exportable table
        $('.js-exportable').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    });
    //
    //
    var table = $('#table-band').DataTable( {
        "responsive": true,
        "serverSide": true,
        "ajax": {
            "url": "/api/admin/band/datatable",
            "type": "POST"
        },
        columns: [
            { data: "id" },
            { data: "nama" },
            { data: "publish",
                render: function (data, type, row, meta) {
                    if (data == 1){
                        return "Publish"
                    }else {
                        return "Unpublish"
                    }
                }},

            { "defaultContent": "<div class='icon-button-demo '>" +
                    "<button title='Edit' class='btn btn-success btn-circle waves-effect waves-circle waves-float  btn-edit'>  <i class=\"material-icons\">edit</i></button>"+
                    "<button title='Foto' class='btn btn-danger btn-circle waves-effect waves-circle waves-float btn-foto' >  <i class=\"material-icons\">image</i></button>"+
                    "<button title='Publish' class='btn btn-primary btn-circle waves-effect waves-circle waves-float btn-publish' >  <i class=\"material-icons\">publish</i></button>"+

                    "</div>"}
            ],
    } );
    $(".btn-tambah").click(function (e) {
       $(".btn-delete").hide();
        $("#frmAddBand").trigger("reset");

    });
    $("#frmAddBand").submit(function (e) {
        e.preventDefault();
        $('.page-loader-wrapper').fadeIn();
        var method = $(this).attr("method");
        var formData = new FormData(this);
        var modal = $("#tambahBandModal");
        id = $("#id").val();
        var url = "/api/admin/admin/create";
        if (id != "") {
            url = "/api/admin/band/update";
        }

        $.ajax({
            url : url,
            processData: false,
            contentType: false,
            data:formData,
            type : method,
            success : function (r) {
                $('.page-loader-wrapper').fadeOut();
                $(this).trigger("reset");
                modal.modal('hide');

                table.ajax.reload();
                if (r.code == 200){
                    showNotification("alert-success",r.msg,
                        "bottom","center","","",);

                }else{
                    showNotification("alert-danger",r.msg,
                        "bottom","center","","",);

                }
            },
            error : function (r) {
                $('.page-loader-wrapper').fadeOut();
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
            }
        });
    });

    $('table tbody').on( 'click', '.btn-edit', function (e) {
        var id = table.row($(this).parents('tr')).data().id;
        $(".title-modal").html("Edit Band");
        $(".btn-delete").show();
        $("#frmAddBand").trigger("reset");

        $.ajax({
            url : "api/admin/band/getById",
            data : {id:id},
            type : "POST",
            success : function (r) {

                $("#tambahBandModal").modal("toggle");
                $("#id").val(r.data.id);
                $("#nama").val(r.data.nama);
                $("#facebook").val(r.data.facebook);
                $("#twitter").val(r.data.twitter);
                $("#instagram").val(r.data.instagram);
                $("#youtube").val(r.data.youtube);
                $("#deskripsi").val(r.data.deskripsi);
            },
            error : function (r) {
                $('.page-loader-wrapper').fadeOut();
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
            }
        });
    });
    $('table tbody').on( 'click', '.btn-publish', function (e) {
        var id = table.row($(this).parents('tr')).data().id;
        $.ajax({
            url : "api/admin/band/publish",
            data : {id:id},
            type : "POST",
            success : function (r) {
                table.ajax.reload();
                showNotification("alert-success",r.msg,
                    "bottom","center","","",);
            },
            error : function (r) {
                $('.page-loader-wrapper').fadeOut();
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
            }

        });
    });

});