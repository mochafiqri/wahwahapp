$(function () {
    initSparkline();

    $("#export-order").click(function (e) {
        e.preventDefault()
        var url = '../api/admin/export/order?id_event='+$("#id_event").val()
        window.open(url);
    })

    $("#export-tiket").click(function (e) {
        e.preventDefault()
        var url = '../api/admin/export/tiket?id_event='+$("#id_event").val()
        window.open(url);
    })

    $("#export-catatan_keuangan").click(function (e) {
        e.preventDefault()
        var url = '../api/admin/export/keuangan?id_event='+$("#id_event").val()
        window.open(url);
    })


});

function initSparkline() {
    $(".sparkline").each(function () {
        var $this = $(this);
        $this.sparkline('html', $this.data());
    });
}