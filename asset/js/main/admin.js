$(document).ready(function() {
    $(function () {
        //Exportable table
        $('.js-exportable').DataTable({
            dom: 'Bfrtip',
            responsive: true,
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
        });
    });
    //
    //
    var table = $('#table-band').DataTable( {
        "responsive": true,
        "serverSide": true,
        "ajax": {
            "url": "/api/admin/admin/datatable",
            "type": "POST"
        },
        columns: [
            { data: "id" },
            { data: "nama" },
            { data: "username" },
            { data: "email" },
            { data: "status",
                render: function (data, type, row, meta) {
                    if (data == 1){
                        return "Publish"
                    }else {
                        return "Unpublish"
                    }
                }},

            { "defaultContent": "<div class='icon-button-demo '>" +
                    "<button title='Edit' class='btn btn-success btn-circle waves-effect waves-circle waves-float  btn-edit'>  <i class=\"material-icons\">edit</i></button>"+

                    "</div>"}
            ],
    } );
    $(".btn-tambah").click(function (e) {
       $(".btn-delete").hide();
        $("#frmAddAdmin").trigger("reset");

    });
    $("#frmAddAdmin").submit(function (e) {
        e.preventDefault();
        $('.page-loader-wrapper').fadeIn();
        var method = $(this).attr("method");
        var formData = new FormData(this);
        var modal = $("#tambahAdminModal");
        id = $("#id").val();
        var url = "/api/admin/admin/create";
        if (id != "") {
            url = "/api/admin/admin/update";
        }

        $.ajax({
            url : url,
            processData: false,
            contentType: false,
            data:formData,
            type : method,
            success : function (r) {
                $('.page-loader-wrapper').fadeOut();
                $(this).trigger("reset");
                modal.modal('hide');

                table.ajax.reload();
                if (r.code == 200){
                    showNotification("alert-success",r.msg,
                        "bottom","center","","",);

                }else{
                    showNotification("alert-danger",r.msg,
                        "bottom","center","","",);

                }
            },
            error : function (r) {
                $('.page-loader-wrapper').fadeOut();
                showNotification("alert-danger",r.msg,
                    "bottom","center","","",);
            }
        });
    });

    // $('table tbody').on( 'click', '.btn-edit', function (e) {
    //     var id = table.row($(this).parents('tr')).data().id;
    //     $(".title-modal").html("Edit Admin");
    //     $(".btn-delete").show();
    //     $("#frmAddAdmin").trigger("reset");
    //
    //     $.ajax({
    //         url : "api/admin/band/getById",
    //         data : {id:id},
    //         type : "POST",
    //         success : function (r) {
    //
    //             $("#tambahBandModal").modal("toggle");
    //             $("#id").val(r.data.id);
    //             $("#nama").val(r.data.nama);
    //             $("#facebook").val(r.data.facebook);
    //             $("#twitter").val(r.data.twitter);
    //             $("#instagram").val(r.data.instagram);
    //             $("#youtube").val(r.data.youtube);
    //             $("#deskripsi").val(r.data.deskripsi);
    //         },
    //         error : function (r) {
    //             $('.page-loader-wrapper').fadeOut();
    //             showNotification("alert-danger",r.msg,
    //                 "bottom","center","","",);
    //         }
    //     });
    // });
    // $('table tbody').on( 'click', '.btn-publish', function (e) {
    //     var id = table.row($(this).parents('tr')).data().id;
    //     $.ajax({
    //         url : "api/admin/band/publish",
    //         data : {id:id},
    //         type : "POST",
    //         success : function (r) {
    //             table.ajax.reload();
    //             showNotification("alert-success",r.msg,
    //                 "bottom","center","","",);
    //         },
    //         error : function (r) {
    //             $('.page-loader-wrapper').fadeOut();
    //             showNotification("alert-danger",r.msg,
    //                 "bottom","center","","",);
    //         }
    //
    //     });
    // });

});