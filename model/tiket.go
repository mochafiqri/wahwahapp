package model

import (
	"fmt"
	"github.com/bandros/framework"
	"strconv"
)

type TiketModel struct {
	Id      	 string `form:"id"`
	IdEvent      string `form:"id_event"`
	IdKategori	 string `form:"id_kategori"`
}

func TiketAdd (tiket TiketModel) (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	tmpTiket := []string{
		tiket.IdEvent,
		tiket.IdKategori,
	}

	db.Call("insert_tiket", tmpTiket)
	data, err := db.Row()
	db.Clear()
	return data, err
}

func TiketKategoridatatable(data DatatablesSource,idEvent string) (*DatatablesResult, error){
	result := DatatablesResult{}
	result.Draw = data.Draw

	fields := []string{
		"a.id",
		"a.nama",
	}

	db:= framework.Database{}
	defer db.Close()

	db.Select("count(id) num")
	db.From("tiket_kategori a").
		Where("a.id_event",idEvent)
	count,err := db.Row()
	if err != nil {
		return nil,err
	}
	rt, _ := strconv.Atoi(count["num"].(string))

	result.RecordsTotal =rt

	if data.Keyword != "" {
		db.StartGroup("AND")
		for _, v := range fields {
			db.WhereOr(v+" like", "%"+data.Keyword+"%")
		}
		db.EndGroup()

	}
	count,err = db.Row()
	if err != nil {
		return nil,err
	}
	rf, _ := strconv.Atoi(count["num"].(string))

	result.RecordsFiltered =rf

	db.Select("a.*,(SELECT count(id) FROM tiket_main tm WHERE tm.id_kategori = a.id AND status = 0 ) sisa_tiket")
	db.Limit(data.Length, data.Start)
	db.OrderBy(fields[data.OrdCol], data.OrdDir)
	dt, err := db.Result()
	if dt == nil {
		result.Data = []map[string]interface{}{}
	} else {
		result.Data = dt
	}
	return &result, nil
}

func TiketKategoriAdd (insert map[string]interface{})(interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.From("tiket_kategori")
	return db.Insert(insert)
}

func TiketKategoriUpdate (update map[string]interface{},id_tiket_k string)(error){
	var db = framework.Database{}
	defer db.Close()

	db.From("tiket_kategori")
	db.Where("id",id_tiket_k)
	return db.Update(update)
}

func TiketKategoriGet (idEvent string)([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()
	db.Select("id,nama,harga")
	db.From("tiket_kategori")
	db.Where("id_event",idEvent)
	db.Where("status",1)
	return db.Result()
}

func TiketKategoriGetById (id string)(map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()
	db.Select("id,nama,harga,id_event")
	db.From("tiket_kategori")
	db.Where("id",id)
	return db.Row()
}

func GetTiketHomepage() ([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("tm.id,tk.nama,tk.harga,em.nama nama_event ")
	db.From("tiket_main tm").
		Join("tiket_kategori tk","tm.id_kategori = tk.id","").
		Join("event_main em" ,"em.id = tk.id_event","")

	db.Where("tm.status",1)
	return db.Result()
}


func TiketSoldByidEventDatatable(idEvent string,data DatatablesSource) (*DatatablesResult,error){
	result := DatatablesResult{}
	result.Draw = data.Draw
	fields := []string{
		"tm.id",
		"tk.nama",
		"tk.harga",
		"em.nama",
	}

	db:= framework.Database{}
	defer db.Close()

	db.Select("count(tm.id) num")
	db.From("tiket_main tm").
		Join("tiket_kategori tk","tm.id_kategori = tk.id","").
		Join("order_main om","tm.id_order = om.id","").
		Join("user_main um","om.id_user = um.id","")

	db.Where("tm.status >=",1)
	db.Where("tk.id_event",idEvent)
	count,err := db.Row()
	if err != nil {
		return nil,err
	}
	rt, _ := strconv.Atoi(count["num"].(string))

	result.RecordsTotal =rt

	if data.Keyword != "" {
		db.StartGroup("AND")
		for _, v := range fields {
			db.WhereOr(v+" like", "%"+data.Keyword+"%")
		}
		db.EndGroup()

	}
	count,err = db.Row()
	if err != nil {
		return nil,err
	}
	rf, _ := strconv.Atoi(count["num"].(string))

	result.RecordsFiltered =rf

	db.Select("tm.id id_tiket,tk.nama nama_kategori,tk.harga,tm.status,um.nama,tm.status")
	db.Limit(data.Length, data.Start)
	db.OrderBy(fields[data.OrdCol], data.OrdDir)
	dt, err := db.Result()
	if dt == nil {
		result.Data = []map[string]interface{}{}
	} else {
		result.Data = dt
	}
	return &result, err
}


func TiketSoldByidEvent(idEvent string) ([]map[string]interface{},error){
	var db =framework.Database{}
	defer db.Close()

	db.Select("tm.id_tiket,tm.nama,tm.harga,tm.status,um.nama")
	db.From("tiket_main tm").
		Join("tiket_kategori tk","tm.id_kategori = tk.id","").
		Join("order_main om","tm.id_order = om.id","").
		Join("user_main um","od.id_user = um.id","")

	db.Where("tk.status",1)

	return db.Result()
}

func TiketKetersediaan(idTiketKategori string)(map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("count(id) jml").
		From("tiket_main").
		Where("status ", 0).
		Where("id_kategori",idTiketKategori)
		db.StartGroup("AND")
		db.Where("id_order raw"," id_order = '' ").
		WhereOr("id_order raw","id_order IS NULL")
		db.EndGroup()
	data,err :=  db.Row()
	return data,err
}

func TiketSetOrder(db framework.Database,idTiketKategori string,qty int,idOrder string) error{


	db.Select("id").
		From("tiket_main").
		Where("status ", 0).
		Where("id_kategori",idTiketKategori)
	db.StartGroup("AND")
	db.Where("id_order raw"," id_order = '' ").
		WhereOr("id_order raw","id_order IS NULL")
	db.EndGroup()
		db.OrderBy("id","asc")

		db.Limit(qty,0)
	tiket,err := db.Result()
	fmt.Println(db.QueryView())
	fmt.Println(tiket)
	if err != nil {
		return err
	}
	var update []map[string]interface{}
	for _,v := range tiket {
		update = append(update,map[string]interface{}{
			"id" : v["id"],
			"id_order" : idOrder,
		})
	}
	err = db.UpdateBatch(update,"id")
	return err
}

func TiketByIdOrder (idOrder string) ([]map[string]interface{},error){
	var db = framework.Database{}
	db.Close()

	db.From("tiket_main").
		Where("id_order",idOrder)
	return db.Result()
}

func TiketByIdTiket (idTiket string) (map[string]interface{},error){
	var db = framework.Database{}
	db.Close()
	db.Select("tm.id id_tiket,om.id id_order,om.tanggal, um.nama, um.no_hp, um.alamat, tm.status," +
		"tk.id_event,tk.harga")
	db.From("tiket_main tm").
		Join("tiket_kategori tk","tm.id_kategori = tk.id","").
		Join("order_main om","tm.id_order = om.id","LEFT").
		Join("user_main um","om.id_user = um.id","LEFT").
		Where("tm.id",idTiket)
	data,err :=  db.Row()
	return data,err
}

func TiketUpdate (idTiket string, update map[string]interface{} )error{
	var db =framework.Database{}
	defer db.Close()

	db.From("tiket_main").
		Where("id",idTiket)
	return db.Update(update)
}

func TiketUpdateDB (db framework.Database,idTiket string, update map[string]interface{} )error{


	db.From("tiket_main").
		Where("id",idTiket)
	return db.Update(update)
}

func TiketUpdateConfirm (idOrder string) error{
	var db =framework.Database{}
	defer db.Close()

	db.From("tiket_main").
		Where("id_order",idOrder)
	return db.Update(map[string]interface{}{
		"status": 1})

}

func TiketCancelByIdOrder (idOrder string) error{
	var db = framework.Database{}
	defer db.Close()

	db.From("tiket_main").
		Where("id_order",idOrder)

	return db.Update(map[string]interface{}{
		"id_order" : "",
	})
}

func TiketLastCheckin() ([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("tm.id, tk.nama, tm.checkin,um.nama nama_pengunjung").From("tiket_main tm").
		Join("tiket_kategori tk","tm.id_kategori = tk.id","").
		Join("order_main om","tm.id_order = om.id","").
		Join("user_main um","um.id = om.id_user","").
		Where("tm.checkin raw","tm.checkin IS NOT NULL").
		Where("tm.status ",2)
	db.Limit(5,0)
	return db.Result()
}

func TiketCountByStatus(idEvent string,status []string) (int,error){
	var db = framework.Database{}
	defer db.Close()
	db.Select("count(tm.id) jml").From("tiket_main tm").
		Join("tiket_kategori tk","tm.id_kategori = tk.id","")
	db.Where("tk.id_event",idEvent)
	db.WhereIn("tm.status",status)
	data,err := db.Row()
	if (err != nil || data == nil) {
		return 0, err
	}
	fmt.Println(data)
	jml,_ := strconv.Atoi(data["jml"].(string))
	return jml,err
}

func TiketHomepage () ([]map[string]interface{},error) {
	var db = framework.Database{}
	defer db.Close()

	db.Select("tk.nama nama_tiket,tk.harga,tk.status status_kategori,em.nama,em.id id_event")
	db.From("tiket_kategori tk").
		Join("event_main em","tk.id_event = em.id","")
	db.Where("em.status <=",3)
	db.Where("publish", 1)
	db.OrderBy("em.id","asc")
	db.Limit(5, 0)
	res, err := db.Result()
	return res, err
}

func TiketKategoriById (idKategori string) (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()
	db.Select("id,status")
	db.From("tiket_kategori tk").
		Where("id",idKategori)

	return db.Row()

}

func TiketPenjualanByIdkategori (idEvent string)  ([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("count(tm.id) jml,tk.nama").
		From("tiket_main tm").
		Join("order_main om","tm.id_order = om.id","").
		Join("tiket_kategori tk","tm.id_kategori = tk.id","")
	db.Where("tk.id_event",idEvent).
		Where("tm.status >=","1").
		Where("om.status","3")
	db.GroupBy("tm.id_kategori")

	data,err := db.Result()
	return data,err

}

func TiketByEvent (idEvent string) ([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("tm.id,tm.checkin,tm.status,um.nama nama_pembeli, tk.nama nama_kategori,om.id id_order").
		From("tiket_main tm").
		Join("order_main om","tm.id_order = om.id","").
		Join("user_main um","om.id_user = um.id","").
		Join("tiket_kategori tk","tm.id_kategori = tk.id","")
	db.Where("om.id_event",idEvent).
		Where("om.status","3")
	db.OrderBy("tm.id","asc")
	return db.Result()
}