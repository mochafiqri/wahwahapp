package model

import (
	"github.com/bandros/framework"
	"strconv"
)

type ModelEvent struct {
	Id        string `form:"id"`
	Nama      string `form:"nama"`
	WktMulai  string `form:"wkt_mulai"`
	WktAkhir  string `form:"wkt_akhir"`
	IdTempat  string `form:"id_tempat"`
	Deskripsi string `form:"deskripsi"`
	Banner    string `form:"banner"`
}

func Eventdatatable(data DatatablesSource) (*DatatablesResult, error) {
	result := DatatablesResult{}
	result.Draw = data.Draw

	fields := []string{
		"em.id",
		"em.nama",
		"em.deskripsi",
	}

	db := framework.Database{}
	defer db.Close()

	db.Select("count(id) num")
	db.From("event_main em")
	count, err := db.Row()
	if err != nil {
		return nil, err
	}
	rt, _ := strconv.Atoi(count["num"].(string))

	result.RecordsTotal = rt

	if data.Keyword != "" {
		db.StartGroup("AND")
		for _, v := range fields {
			db.WhereOr(v+" like", "%"+data.Keyword+"%")
		}
		db.EndGroup()

	}
	count, err = db.Row()
	if err != nil {
		return nil, err
	}
	rf, _ := strconv.Atoi(count["num"].(string))

	result.RecordsFiltered = rf

	db.Select("em.id,em.nama,em.deskripsi,em.status,em.publish")
	db.Limit(data.Length, data.Start)
	db.OrderBy(fields[data.OrdCol], data.OrdDir)
	dt, err := db.Result()
	if dt == nil {
		result.Data = []map[string]interface{}{}
	} else {
		result.Data = dt
	}
	return &result, nil
}

func GetEventList() ([]map[string]interface{}, error) {
	var db = framework.Database{}
	defer db.Close()

	db.Select("em.*,DATE_FORMAT(em.waktu_mulai,'%W, %d %M %Y') tanggal_mulai,tm.nama_tempat," +
		"max(tk.harga) max_harga,min(tk.harga) min_harga")
	db.From("event_main em").
		Join("tempat_main tm","em.id_tempat = tm.id","").
		Join("tiket_kategori tk","em.id = tk.id_event","LEFT")
	db.Where("em.publish", 1)
	db.Where("em.status !=", 1)
	db.Where("em.status <", 6)
	db.GroupBy("em.id").
		OrderBy("em.id","desc")
	res, err := db.Result()

	return res, err
}

func GetEventHomepage() ([]map[string]interface{}, error) {
	var db = framework.Database{}
	defer db.Close()

	db.Select("em.*,DATE_FORMAT(em.waktu_mulai,'%W, %d %M %Y') tanggal_mulai,tm.nama_tempat," +
		"max(tk.harga) max_harga,min(tk.harga) min_harga")
	db.From("event_main em").
		Join("tempat_main tm","em.id_tempat = tm.id","").
		Join("tiket_kategori tk","em.id = tk.id_event","LEFT")
	db.Where("em.publish", 1).
		Where("em.status",2)
	db.GroupBy("em.id").
		OrderBy("em.id","desc").
		Limit(3,0)
	res, err := db.Result()
	db.Clear()
	if len(res) == 0 {
		db.Select("em.*,DATE_FORMAT(em.waktu_mulai,'%W, %d %M %Y') tanggal_mulai,tm.nama_tempat," +
			"max(tk.harga) max_harga,min(tk.harga) min_harga")
		db.From("event_main em").
			Join("tempat_main tm","em.id_tempat = tm.id","").
			Join("tiket_kategori tk","em.id = tk.id_event","LEFT")
		db.Where("em.publish", 1)
		db.GroupBy("em.id").
			OrderBy("em.id","desc")
		tmpRes,err := db.Row()
		if err != nil {
			return res,err
		}
		res = append(res,tmpRes)
	}
	return res, err
}


func GetEventDashboard() ([]map[string]interface{}, error) {
	var db = framework.Database{}
	defer db.Close()

	db.Select("em.*,DATE_FORMAT(em.waktu_mulai,'%W, %d %M %Y') tanggal_mulai,tm.nama_tempat")
	db.From("event_main em").
		Join("tempat_main tm","em.id_tempat = tm.id","").
		Join("tiket_kategori tk","em.id = tk.id_event","LEFT")
	db.Where("em.publish", 1)
	db.GroupBy("em.id").
		OrderBy("em.id","desc").
		Limit(3,0)
	res, err := db.Result()
	return res, err
}

func EventInsert(insert map[string]interface{}) (interface{}, error) {
	var db = framework.Database{}
	defer db.Close()

	db.From("event_main")
	return db.Insert(insert)
}

func EventGetById(id string, sel string) (map[string]interface{}, error) {
	db := framework.Database{}
	defer db.Close()
	db.Select(sel)
	db.From("event_main")
	db.Where("id", id)
	return db.Row()
}

func EventDetail(idEvent string) (map[string]interface{}, error) {
	db := framework.Database{}
	defer db.Close()

	db.Select("em.id,em.nama,em.deskripsi,em.status,"+
		"DATE_FORMAT(em.waktu_mulai, '%d %M %Y') tanggal_mulai,"+
		"DATE_FORMAT(em.waktu_mulai, '%H:%i') jam_mulai,"+
		"DATE_FORMAT(em.waktu_berakhir, '%d %M %Y') tanggal_selesai,"+
		"DATE_FORMAT(em.waktu_berakhir, '%H:%i') jam_selesai,"+
		"tm.nama_tempat,tm.alamat,tm.kota,tm.geo_location," +
		"max(tk.harga) max_harga,min(tk.harga) min_harga").From("event_main em").
		Join("tempat_main tm", "em.id_tempat = tm.id", "").
		Join("tiket_kategori tk","em.id = tk.id_event","LEFT")
	db.GroupBy("em.id")
	db.Where("em.id", idEvent)
	data, err := db.Row()
	if err != nil {
		return nil, err
	}

	//tiket
	db.Clear()
	db.Select("tk.id,tk.nama,tk.harga")
	db.From("tiket_kategori tk")
	db.Where("id_event", idEvent)
	db.Where("status", 1).
		Where("status raw", "(select count(tm.id) from tiket_main tm where tm.id_kategori = tk.id and (tm.id_order is null or tm.id_order = ''))  > 0")
	tiket, err := db.Result()
	if err != nil {
		return nil, err
	}
	data["tiket"] = tiket

	//performer
	db.Clear()
	db.Select("bm.nama,eb.rundown," +
		"DATE_FORMAT(eb.rundown, '%W') hari,"+
		"DATE_FORMAT(eb.rundown, '%H:%i') jam_mulai,bf.foto ").
		From("event_band eb").
		Join("band_main bm","eb.id_band = bm.id","").
		Join("band_foto bf","bf.id_band = bm.id  ","").
		Where("bf.main",1).
		Where("id_event",idEvent).
		OrderBy("eb.rundown","asc")
	band, err := db.Result()
	if err != nil {
		return nil, err
	}
	data["band"] = band
	var poster = ""
	dataPoster,_ := GetEventPosterMainByIdEvent(idEvent)
	if dataPoster != nil {
		poster = dataPoster["foto"].(string)
	}
	data["poster"] = poster
	return data, err
}

func EventUpdate(idEvent string, update map[string]interface{}) error {
	var db = framework.Database{}
	defer db.Close()

	db.From("event_main")
	db.Where("id", idEvent)
	err := db.Update(update)
	return err
}
