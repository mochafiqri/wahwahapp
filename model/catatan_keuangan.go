package model

import (
	"fmt"
	"github.com/bandros/framework"
	"strconv"
)

func GetCatatanKeuanganSaldo (idEvent string) (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("saldo").From("catatan_uang")
	db.Where("id_event",idEvent)
	db.OrderBy("id","desc")
	return db.Row()
}

func GetCatataKeuanganKredit (idEvent string) (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("SUM(kredit) kredit").From("catatan_uang")
	db.Where("id_event",idEvent)
	db.OrderBy("id","desc")
	return db.Row()
}
func GetCatataKeuanganDebit (idEvent string) (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("SUM(debit) debit").From("catatan_uang")
	db.Where("id_event",idEvent)
	db.OrderBy("id","desc")
	return db.Row()
}


func CatataKeuanganEventDtb(data DatatablesSource ,idEvent string)  (*DatatablesResult, error){
	result := DatatablesResult{}
	result.Draw = data.Draw

	fields := []string{
		"ck.saldo",
		"kt.nama",
	}

	db:= framework.Database{}
	defer db.Close()

	db.Select("count(ck.id) num")
	db.From("catatan_uang ck").
		Where("ck.id_event",idEvent)
	count,err := db.Row()
	if err != nil {
		return nil,err
	}
	rt, _ := strconv.Atoi(count["num"].(string))

	result.RecordsTotal =rt

	if data.Keyword != "" {
		db.StartGroup("AND")
		for _, v := range fields {
			db.WhereOr(v+" like", "%"+data.Keyword+"%")
		}
		db.EndGroup()

	}
	count,err = db.Row()
	if err != nil {
		return nil,err
	}
	rf, _ := strconv.Atoi(count["num"].(string))

	result.RecordsFiltered =rf

	db.Select("ck.id,kt.nama kategori,ck.kredit,ck.debit,ck.saldo,ck.ket," +
		"DATE_FORMAT(ck.create_at, '%d %M %Y') tanggal").
		From("catatan_uang ck").
		Join("catatan_uang_kategori kt","ck.id_kategori = kt.id","")
	db.Limit(data.Length, data.Start)
	db.OrderBy(fields[data.OrdCol], data.OrdDir)
	dt, err := db.Result()
	if dt == nil {
		result.Data = []map[string]interface{}{}
	} else {
		result.Data = dt
	}
	return &result, err
}

func CatatanKeuanganInsert (insert map[string]interface{}) (interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.From("catatan_uang")
	return db.Insert(insert)
}

func GetCatatanKeuanganByIdKategori (idEvent string,idKategori []string) (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.From("catatan_uang").
		Where("id_kategori",idKategori).
		Where("id_event",idEvent)
	return db.Row()
}

func GetDebitKeunanganDashboard(idEvent string) ([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("sum(debit) jml, kt.id,kt.nama")
	db.From("catatan_uang ck").
		Join("catatan_uang_kategori kt","ck.id_kategori = kt.id","").
		Where("ck.id_event",idEvent)
	db.GroupBy("kt.id")
	data,err := db.Result()
	fmt.Println(db.QueryView())
	return data,err
}

func GetKreditKeunanganDashboard(idEvent string) ([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("sum(kredit) jml, kt.id,kt.nama")
	db.From("catatan_uang ck").
		Join("catatan_uang_kategori kt","ck.id_kategori = kt.id","").
		Where("ck.id_event",idEvent)
	db.GroupBy("kt.id")
	return db.Result()
}

func GetCatatanKeuanganByIdEvent (idEvent string) ([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("kt.nama kategori, kredit,debit, saldo, create_at tanggal, ket")
		db.From("catatan_uang ck").
		Join("catatan_uang_kategori kt","ck.id_kategori = kt.id","").
		Where("ck.id_event",idEvent)
	return db.Result()

}