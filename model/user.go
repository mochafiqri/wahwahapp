package model

import (
	"github.com/bandros/framework"
)

type UserModel struct {

	Id string `form:"id"`
	Nama string `form:"nama"`
	Hp string `form:"no_hp"`
	Email string `form:"email"`
	Alamat string `form:"alamat"`
	Password string `form:"password"`
	Tgl_lahir string `form:"tgl_lahir"`

}


func CheckNumber (number string,email string) (map[string]interface{},error) {
	var db = framework.Database{}
	defer db.Close()

	db.Select("id,password").From("user_main")
	db.Where("no_hp",number)
	db.WhereOr("email",email)
	dt, nil := db.Row()
	return  dt,nil
}

func Register (user UserModel) (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	tmpUser := []string{
		user.Nama,
		user.Email,
		user.Hp,
		user.Alamat,
		framework.Password(user.Password),
		user.Tgl_lahir,
	}

	db.Call("insert_user", tmpUser)
	data, err := db.Row()
	if err != nil {
		return nil, err
	}
	db.Clear()
	db.Select("id,nama,email,no_hp,foto")
	db.From("user_main")
	db.Where("id", data["id"])
	res, err := db.Row()
	if err != nil {
		return nil, err
	}
	return res, err
}

func UserGet(user string) (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("id,nama,email,no_hp,foto,password")
	db.From("user_main")
	db.Where("email",user)
	db.WhereOr("no_hp",framework.NumberPhone(user))
	res,err:= db.Row()
	return res,err
}

func UserById(id string,sel string) (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select(sel).From("user_main")
	db.Where("id",id)
	data,err := db.Row()
	return data,err
}
