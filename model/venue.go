package model

import "github.com/bandros/framework"




func VenuneData() ([]map[string]interface{},error){

	var db = framework.Database{}
	defer db.Close()

	db.Select("id,nama_tempat")
	db.From("tempat_main").Where("publish",1)
	return db.Result()
}