package model

import (
	"github.com/bandros/framework"
)

func CatatanKeuanganKategoriAdd (insert map[string]interface{})(interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.From("catatan_uang_kategori")
	return db.Insert(insert)
}

func CatatanKeuanganKategoriGet (tipe string)([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()
	db.Select("id,nama,type")
	db.From("catatan_uang_kategori")
	if tipe != "" {
		db.Where("type",tipe)
	}
	db.Where("publish",1)
	return db.Result()
}