package model

import (
	"github.com/bandros/framework"
	"strconv"
)

func AddEventBand (db framework.Database,insert map[string]interface{}) error{
	db.From("event_band")
	_,err := db.Insert(insert)
	return err
}

func GetEventBandByIdEvent(idEvent string ) ([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("*").From("event_band")
	db.Where("id_event",idEvent).
		Where("rundown",1)
	return db.Result()
}

func InsertBand(insert map[string]interface{}) (interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.From("event_band")
	return db.Insert(insert)
}
func EventBandUpdate(idEvent string,update map[string]interface{}) error{
	var db = framework.Database{}
	defer db.Close()

	db.From("event_band")
	db.Where("id_event",idEvent)
	db.Where("rundown",1)
	return db.Update(update)
}

func EventBanddatatable(data DatatablesSource, idEvent string) (*DatatablesResult, error){
	result := DatatablesResult{}
	result.Draw = data.Draw

	fields := []string{
		"eb.id",
		"r.review",
		"r.id_user",
		"um.nama",
	}

	db:= framework.Database{}
	defer db.Close()

	db.Select("count(eb.id) num")
	db.From("event_band eb").
		Where("id_event",idEvent)

	count,err := db.Row()
	if err != nil {
		return nil,err
	}
	rt, _ := strconv.Atoi(count["num"].(string))

	result.RecordsTotal =rt

	if data.Keyword != "" {
		db.StartGroup("AND")
		for _, v := range fields {
			db.WhereOr(v+" like", "%"+data.Keyword+"%")
		}
		db.EndGroup()

	}
	count,err = db.Row()
	if err != nil {
		return nil,err
	}
	rf, _ := strconv.Atoi(count["num"].(string))

	result.RecordsFiltered =rf

	db.Select("eb.id,bm.nama,eb.rundown").
		Join("band_main bm","eb.id_band = bm.id","")

	db.Limit(data.Length, data.Start)
	db.OrderBy(fields[data.OrdCol], data.OrdDir)
	dt, err := db.Result()
	if dt == nil {
		result.Data = []map[string]interface{}{}
	} else {
		result.Data = dt
	}
	return &result, nil
}
