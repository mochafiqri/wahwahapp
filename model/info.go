package model

import "github.com/bandros/framework"

func TambahInfo(insert map[string]interface{}) error {
	var db = framework.Database{}
	defer db.Close()

	db.From("event_info")
	_,err := db.Insert(insert)
	return err
}

func GetInfoById(idInfo string) (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("ei.id,ei.id_event,ei.banner,ei.info,ei.status,em.nama")
	db.From("event_info ei").
		Join("event_main em","ei.id_event = em.id","")
	db.Where("ei.id",idInfo)
	return db.Row()
}
