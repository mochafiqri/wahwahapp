package model

import "github.com/bandros/framework"

func BannerGet(sel string, order, ket string) ([]map[string]interface{}, error) {
	db := framework.Database{}
	defer db.Close()
	db.Select(sel)
	db.From("banner_main")
	db.Where("publish", 1)
	db.OrderBy(order, ket)
	return db.Result()
}

func BannerAdd(insert map[string]interface{}) (interface{}, error) {
	var db = framework.Database{}
	defer db.Close()
	db.From("banner_main")
	return db.Insert(insert)
}

func BannerDelete(id string) error {
	db := framework.Database{}
	defer db.Close()
	db.From("banner_main")
	db.Where("id", id)
	err := db.Update(map[string]interface{}{"status": 0})
	return err
}

func BannerUpdate(update map[string]interface{}, id string) error {
	db := framework.Database{}
	defer db.Close()
	db.From("banner_main")
	db.Where("id", id)
	return db.Update(update)
}

func BannerById(id string, sel string) (map[string]interface{}, error) {
	db := framework.Database{}
	defer db.Close()
	db.Select(sel)
	db.From("banner_main")
	db.Where("id", id)
	return db.Row()
}

