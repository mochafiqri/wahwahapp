package model

import "github.com/bandros/framework"

func InserEventPoster (insert map[string]interface{}) (interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.From("event_foto")
	return db.Insert(insert)
}

func UpdateEventPoster(update map[string]interface{},id string) error{
	var db = framework.Database{}
	defer db.Close()

	db.From("event_foto").
		Where("id",id)

	return db.Update(update)
}

func GetEventPosterMainByIdEvent (idEvent string) (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("id,foto,main").From("event_foto").
		Where("id_event",idEvent).
		Where("main",1)
	return db.Row()
}