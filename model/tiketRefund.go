package model

import (
	"github.com/bandros/framework"
	"strconv"
)

func TiketRefundInsert (db framework.Database,insert map[string]interface{}) (interface{},error){
	db.From("tiket_refund")
	return db.Insert(insert)
}

func TiketRefunddatatable(data DatatablesSource,status []string) (*DatatablesResult, error){
	result := DatatablesResult{}
	result.Draw = data.Draw

	fields := []string{
		"tr.id",
		"tr.tgl_request",
		"um.nama",
		"tr.status",
		"tr.bank",
	}

	db:= framework.Database{}
	defer db.Close()

	db.Select("count(tr.id) num")
	db.From("tiket_refund tr").
		Join("tiket_main tm","tr.id_tiket = tm.id","LEFT").
		WhereIn("tr.status",status)
	count,err := db.Row()
	if err != nil {
		return nil,err
	}
	rt, _ := strconv.Atoi(count["num"].(string))

	result.RecordsTotal =rt

	if data.Keyword != "" {
		db.StartGroup("AND")
		for _, v := range fields {
			db.WhereOr(v+" like", "%"+data.Keyword+"%")
		}
		db.EndGroup()

	}
	count,err = db.Row()
	if err != nil {
		return nil,err
	}
	rf, _ := strconv.Atoi(count["num"].(string))

	result.RecordsFiltered =rf

	db.Select("tr.id,tr.status,um.nama nama_user,tr.tgl_request, tr.bank," +
		"CONCAT(tr.no_rekening,' - ',tr.atas_nama) no_rek").
		Join("user_main um","tr.id_user = um.id","")
	db.Limit(data.Length, data.Start)
	db.OrderBy("tr.status","asc")
	dt, err := db.Result()
	if dt == nil {
		result.Data = []map[string]interface{}{}
	} else {
		result.Data = dt
	}
	return &result, nil
}


func GetDataRefundByIdRefund (idRefund string) (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("*").
		From("tiket_refund").
		Where("id",idRefund)

	return db.Row()
}

func UpdateDataRefund(idRefund string, update map[string]interface{})error {
	var db = framework.Database{}
	defer db.Close()
	db.From("tiket_refund").
		Where("id",idRefund)

	return db.Update(update)
}

func UpdateDataRefundDb(db framework.Database,idRefund string, update map[string]interface{})error {
	db.From("tiket_refund").
		Where("id",idRefund)

	return db.Update(update)
}

func GetRefundByIdUser(idUser string) ([]map[string]interface{},error){
	var db =framework.Database{}
	defer db.Close()

	db.Select("id,jumlah,tgl_request," +
		"tgl_refund,bank,atas_nama,no_rekening,status").From("tiket_refund")
	db.Where("id_user",idUser)

	return db.Result()
}

func GetRefundByIdTiketIdUser (idTiket,idUser string) (map[string]interface{},error){
	var db =framework.Database{}
	defer db.Close()

	db.Select("id,jumlah,tgl_request," +
		"tgl_refund,bank,atas_nama,no_rekening,status").From("tiket_refund")
	db.Where("id_user",idUser).
		Where("id_tiket",idTiket)
	db.Where("status ",0)

	return db.Row()
}