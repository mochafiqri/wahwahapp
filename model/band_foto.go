package model

import "github.com/bandros/framework"

func GetBandFotoMain(idBand string) (map[string]interface{},error){
	var db =framework.Database{}
	defer db.Close()

	db.Select("foto")
	db.From("band_foto")
	db.Where("main",1)
	db.Where("id_band",idBand)
	return db.Row()
}

func AddFotoMain (insert map[string]interface{}) (interface{},error){
	var db =framework.Database{}
	defer db.Close()

	db.From("band_foto")

	return db.Insert(insert)
}
