package model

import "github.com/bandros/framework"

func ListTipePembayaran () ([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("id,nama_pembayaran,tipe_pembayaran,nama_bank,logo,biaya").
		From("tipe_pembayaran")
	return db.Result()
}

func TipePembayaranById(idTipe string) (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("*").From("tipe_pembayaran").
		Where("id",idTipe)
	return db.Row()
}
