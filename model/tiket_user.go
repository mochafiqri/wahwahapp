package model

import (
	"github.com/bandros/framework"
)

func GetMyTiket(idUser string) ([]map[string]interface{},error){
	var db =framework.Database{}
	defer db.Close()

	db.Select("tm.id , tm.status,tk.nama kategori, em.nama,em.refundable,tk.harga").From("tiket_main tm").
		Join("order_main o","tm.id_order = o.id","").
		Join("tiket_kategori tk","tm.id_kategori = tk.id","").
		Join("event_main em","tk.id_event = em.id","")

	db.Where("o.id_user",idUser).
		Where("o.status",3).
		OrderBy("o.id","desc")

	return db.Result()
}

func GetOrderByIdTiket(idTiket string) (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("tm.id_order").
		From("tiket_main tm").
		Where("tm.id",idTiket)
	return db.Row()
}

func GetTiketByIdOrder(idOrder string) ([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("tm.id id_tiket,tm.id_order,tm.status tiket_status,tk.nama nama_kategori, um.nama, um.tgl_lahir, " +
		"om.status order_status,em.nama nama_event,DATE_FORMAT(em.waktu_mulai,'%W, %d %M %Y') tanggal_mulai  ").
		From("tiket_main tm").
		Join("order_main om","tm.id_order = om.id","").
		Join("tiket_kategori tk","tk.id = tm.id_kategori","").
		Join("event_main em","tk.id_event = em.id","").
		Join("user_main um","om.id_user = um.id","").
		Where("tm.id_order",idOrder)
	return db.Result()
}

func GetTiketRefundByIdOrder(idOrder string) ([]map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("tm.id_order, tk.nama nama_kategori,"+
		"om.status order_status,em.nama nama_event,DATE_FORMAT(em.waktu_mulai,'%W, %d %M %Y') tanggal_mulai  ").
		From("tiket_refund tr").
		Join("tiket_main tm","tr.id_tiket = tm.id","").
		Join("order_main om","tm.id_order = om.id","").
		Join("tiket_kategori tk","tk.id = tm.id_kategori","").
		Join("event_main em","tk.id_event = em.id","").
		Join("user_main um","om.id_user = um.id","").
		Where("tr.id_order",idOrder)
	return db.Result()
}

