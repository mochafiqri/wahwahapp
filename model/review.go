package model

import (
	"github.com/bandros/framework"
	"strconv"
)



func ReviewAdd (insert map[string]interface{}) (interface{},error){

	var db = framework.Database{}
	defer db.Close()
	db.From("review_main")

	return db.Insert(insert)
}

func CheckReview(idEvent,idUser string) (map[string]interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.Select("id").From("review_main").
		Where("id_user",idUser).
		Where("id_event",idEvent)
	return db.Row()
}

func UpdateReview(idReview string, update map[string]interface{})error{
	var db = framework.Database{}
	defer db.Close()

	db.From("review_main").
		Where("id",idReview)
	return db.Update(update)
}



func Reviewdatatable(data DatatablesSource) (*DatatablesResult, error){
	result := DatatablesResult{}
	result.Draw = data.Draw

	fields := []string{
		"r.id",
		"r.review",
		"r.id_user",
		"um.nama",
	}

	db:= framework.Database{}
	defer db.Close()

	db.Select("count(r.id) num")
	db.From("review_main r")
	count,err := db.Row()
	if err != nil {
		return nil,err
	}
	rt, _ := strconv.Atoi(count["num"].(string))

	result.RecordsTotal =rt

	if data.Keyword != "" {
		db.StartGroup("AND")
		for _, v := range fields {
			db.WhereOr(v+" like", "%"+data.Keyword+"%")
		}
		db.EndGroup()

	}
	count,err = db.Row()
	if err != nil {
		return nil,err
	}
	rf, _ := strconv.Atoi(count["num"].(string))

	result.RecordsFiltered =rf

	db.Select("r.id,r.rating,r.review,um.nama").
		Join("user_main um","r.id_user = um.id","")

	db.Limit(data.Length, data.Start)
	db.OrderBy(fields[data.OrdCol], data.OrdDir)
	dt, err := db.Result()
	if dt == nil {
		result.Data = []map[string]interface{}{}
	} else {
		result.Data = dt
	}
	return &result, nil
}

func GetAvgRatingByIdEvent (idEvent string) (map[string]interface{},error) {
	var db = framework.Database{}
	defer db.Close()

	db.Select("avg(rating) rating").
		From("review_main").
		Where("id_event",idEvent)

	return db.Row()
}

func GetRatingPerKategoriByIdEvent (idEvent string) ([]map[string]interface{},error) {
	var db = framework.Database{}
	defer db.Close()

	db.Select("rating,count(id) jml").
		From("review_main").
		GroupBy("rating").
		Where("id_event",idEvent)

	return db.Result()
}

func GetJmlReview (idEvent string) (map[string]interface{},error) {
	var db = framework.Database{}
	defer db.Close()

	db.Select("count(id) jml").
		From("review_main").
		Where("id_event",idEvent)
	return db.Row()
}