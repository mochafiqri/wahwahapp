package model

import (
	"github.com/bandros/framework"
	"strconv"
)

func CekLogin (email,password string) (map[string]interface{},error){
	db := framework.Database{}
	defer db.Close()
	db.Select("id,nama,email,username,level")
	db.From("admin")
	db.StartGroup("AND")
	db.StartGroup("OR")
	db.Where("email", email)
	db.WhereOr("username", email)
	db.EndGroup()
	db.Where("password", framework.Password(password))
	db.EndGroup()
	data, err := db.Row()
	if err != nil {
		return nil, err
	}
	return data, nil
}

func Admindatatable(data DatatablesSource) (*DatatablesResult, error){
	result := DatatablesResult{}
	result.Draw = data.Draw

	fields := []string{
		"a.id",
		"a.nama",
	}

	db:= framework.Database{}
	defer db.Close()




	db.Select("count(id) num")
	db.From("admin a")
	count,err := db.Row()
	if err != nil {
		return nil,err
	}
	rt, _ := strconv.Atoi(count["num"].(string))

	result.RecordsTotal =rt

	if data.Keyword != "" {
		db.StartGroup("AND")
		for _, v := range fields {
			db.WhereOr(v+" like", "%"+data.Keyword+"%")
		}
		db.EndGroup()

	}
	count,err = db.Row()
	if err != nil {
		return nil,err
	}
	rf, _ := strconv.Atoi(count["num"].(string))

	result.RecordsFiltered =rf

	db.Select("a.*")
	db.Limit(data.Length, data.Start)
	db.OrderBy(fields[data.OrdCol], data.OrdDir)
	dt, err := db.Result()
	if dt == nil {
		result.Data = []map[string]interface{}{}
	} else {
		result.Data = dt
	}
	return &result, nil
}


func AdminAdd (insert map[string]interface{}) (interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.From("admin")
	return db.Insert(insert)
}
