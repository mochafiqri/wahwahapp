package model

import "github.com/bandros/framework"

func EventAdminByIdAdminIdEvent (idEvent,idAadmin string) (map[string]interface{},error) {
	var db = framework.Database{}
	defer db.Close()

	db.Select("id")
	db.From("event_admin").
		Where("id_admin",idAadmin).
		Where("id_event",idEvent)
	return db.Row()
}

func EventAdminInsert (insert map[string]interface{}) (interface{},error){
	var db = framework.Database{}
	defer db.Close()

	db.From("event_admin")
	return db.Insert(insert)
}

