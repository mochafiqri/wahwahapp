package model

import (
	"github.com/bandros/framework"
	"math"
	"strconv"
)

type ModelBand struct {
	Id       string `form:"id"`
	Nama     string `form:"nama"`
	Deskripsi string `form:"deskripsi"`
	Facebook string `form:"facebook"`
	Twitter string `form:"twitter"`
	Instagram string `form:"instagram"`
	Youtube string `form:"youtube"`
	Publish string `form:"publish"`
}

func Banddatatable(data DatatablesSource) (*DatatablesResult, error){
	result := DatatablesResult{}
	result.Draw = data.Draw

	fields := []string{
		"a.id",
		"a.nama",
	}

	db:= framework.Database{}
	defer db.Close()




	db.Select("count(id) num")
	db.From("band_main a")
	count,err := db.Row()
	if err != nil {
		return nil,err
	}
	rt, _ := strconv.Atoi(count["num"].(string))

	result.RecordsTotal =rt

	if data.Keyword != "" {
		db.StartGroup("AND")
		for _, v := range fields {
			db.WhereOr(v+" like", "%"+data.Keyword+"%")
		}
		db.EndGroup()

	}
	count,err = db.Row()
	if err != nil {
		return nil,err
	}
	rf, _ := strconv.Atoi(count["num"].(string))

	result.RecordsFiltered =rf

	db.Select("a.*")
	db.Limit(data.Length, data.Start)
	db.OrderBy(fields[data.OrdCol], data.OrdDir)
	dt, err := db.Result()
	if dt == nil {
		result.Data = []map[string]interface{}{}
	} else {
		result.Data = dt
	}
	return &result, nil
}



func BandGetHomepage() ([]map[string]interface{}, error) {
	db := framework.Database{}
	defer db.Close()
	db.Select("bm.id,bm.nama")
	db.From("band_main bm")
	db.Where("bm.publish", 1)
	db.OrderBy("id", "asc")
	data,err :=  db.Result()
	if err != nil {
		return nil, err
	}
	db.Clear()
	var result = []map[string]interface{}{}
	for _,v := range data{
		foto,err :=GetBandFotoMain(v["id"].(string))
		if err != nil {
			return nil, err
		}
		if foto != nil {
			var tmpResult = map[string]interface{}{
				"id"	:v["id"],
				"nama"	:v["nama"],
				"foto"	: foto["foto"],
			}
			result = append(result,tmpResult)
		}

	}
	return result,err
}

func BandListPage(sr DataSource) (DataResult, error) {
	var data = DataResult{}
	data.Length = sr.Length
	db := framework.Database{}
	defer db.Close()
	if sr.Search != "" {
		db.StartGroup("AND")
		db.WhereOr("id like", "%"+sr.Search+"%")
		db.WhereOr("nama like", "%"+sr.Search+"%")
		db.EndGroup()
	}
	db.Select("count(id) num")
	db.From("band_main")
	count, err := db.Row()
	if err != nil {
		return DataResult{}, err
	}
	dt, _ := strconv.Atoi(count["num"].(string))
	data.TotalData = dt
	db.Select("id,nama,deskripsi,facebook,twitter,instagram,youtube,publish").From("band_main").
		Limit(sr.Length, (sr.Page-1)*sr.Length).
		OrderBy("id", "asc")
	result, err := db.Result()
	if err != nil {
		return DataResult{}, err
	}
	data.Data = result
	data.NumPage = int(math.Ceil(float64(data.TotalData) / float64(data.Length)))
	return data, nil
}

func BandSave(insert map[string]interface{}) (interface{}, error) {
	db := framework.Database{}
	defer db.Close()
	db.From("band_main")
	return db.Insert(insert)
}

func BandGetById (id string,sel string) (map[string]interface{},error){
	db := framework.Database{}
	defer db.Close()

	db.Select(sel)
	db.From("band_main")
	db.Where("id",id)
	data,err :=  db.Row()
	return data,err
}

func BandGet (sel string) ([]map[string]interface{},error){
	db := framework.Database{}
	defer db.Close()

	db.Select(sel)
	db.From("band_main").
		Where("publish",1)
	return db.Result()
}

func BandDelete(id string) error {
	db := framework.Database{}
	defer db.Close()

	db.From("band_main")
	db.Where("id", id)
	db.Select("id")
	err := db.Update(map[string]interface{}{"publish": 0})
	return err
}

func BandUpdate(id string, update map[string]interface{}) error {
	db := framework.Database{}
	defer db.Close()
	db.From("band_main")
	db.Where("id", id)
	db.Select("id")
	err := db.Update(update)
	return err
}

func BandEventUpdateRundown (idEventBand, time string) error{
	var db = framework.Database{}
	defer db.Close()
	var update = map[string]interface{}{
		"rundown" : time,
	}
	db.From("event_band").
		Where("id",idEventBand)
	return db.Update(update)
}