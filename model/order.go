package model

import (
	"github.com/bandros/framework"
	"strconv"
)

func InsertOrder(db framework.Database,insert []string) (map[string]interface{}, error) {
	db.Call("insert_order", insert)
	return db.Row()
}

func CekKodeUnik(kodeunik int) (map[string]interface{}, error) {
	var db = framework.Database{}
	defer db.Close()

	db.Select("id").From("order_main")
	db.Where("kode_unik", kodeunik)
	db.Where("tanggal raw", "tanggal >= DATE_SUB(now(), INTERVAL 2 DAY)")
	return db.Row()
}

func OrderPendingDtb(data DatatablesSource) (*DatatablesResult, error) {
	result := DatatablesResult{}
	result.Draw = data.Draw

	fields := []string{
		"om.id",
		"um.nama",
	}

	db := framework.Database{}
	defer db.Close()

	db.Select("count(om.id) num")
	db.From("order_main om").
		Where("om.status", 1).
		WhereOr("om.status", 2)
	count, err := db.Row()
	if err != nil {
		return nil, err
	}
	rt, _ := strconv.Atoi(count["num"].(string))

	result.RecordsTotal = rt

	if data.Keyword != "" {
		db.StartGroup("AND")
		for _, v := range fields {
			db.WhereOr(v+" like", "%"+data.Keyword+"%")
		}
		db.EndGroup()

	}
	count, err = db.Row()
	if err != nil {
		return nil, err
	}
	rf, _ := strconv.Atoi(count["num"].(string))

	result.RecordsFiltered = rf

	db.Select("om.id,om.bukti_foto,om.tanggal,"+
		"om.qty,om.status,um.nama nama_user,"+
		"om.total+om.kode_unik total").
		Join("user_main um", "um.id = om.id_user", "")
	db.Limit(data.Length, data.Start)
	db.OrderBy(fields[data.OrdCol], data.OrdDir)
	dt, err := db.Result()
	if dt == nil {
		result.Data = []map[string]interface{}{}
	} else {
		result.Data = dt
	}
	return &result, nil
}

func OrderSuksesDtb(data DatatablesSource) (*DatatablesResult, error) {
	result := DatatablesResult{}
	result.Draw = data.Draw

	fields := []string{
		"om.id",
		"um.nama",
	}

	db := framework.Database{}
	defer db.Close()

	db.Select("count(om.id) num")
	db.From("order_main om").
		Where("om.status", 3)
	count, err := db.Row()
	if err != nil {
		return nil, err
	}
	rt, _ := strconv.Atoi(count["num"].(string))

	result.RecordsTotal = rt

	if data.Keyword != "" {
		db.StartGroup("AND")
		for _, v := range fields {
			db.WhereOr(v+" like", "%"+data.Keyword+"%")
		}
		db.EndGroup()

	}
	count, err = db.Row()
	if err != nil {
		return nil, err
	}
	rf, _ := strconv.Atoi(count["num"].(string))

	result.RecordsFiltered = rf

	db.Select("om.id,om.bukti_foto,om.tanggal,"+
		"om.qty,om.status,um.nama nama_user,"+
		"om.total+om.kode_unik total").
		Join("user_main um", "um.id = om.id_user", "")
	db.Limit(data.Length, data.Start)
	db.OrderBy(fields[data.OrdCol], data.OrdDir)
	dt, err := db.Result()
	if dt == nil {
		result.Data = []map[string]interface{}{}
	} else {
		result.Data = dt
	}
	return &result, nil
}

func OrderCancelDtb(data DatatablesSource) (*DatatablesResult, error) {
	result := DatatablesResult{}
	result.Draw = data.Draw

	fields := []string{
		"om.id",
		"um.nama",
	}

	db := framework.Database{}
	defer db.Close()

	db.Select("count(om.id) num")
	db.From("order_main om").
		Where("om.status", 3)
	count, err := db.Row()
	if err != nil {
		return nil, err
	}
	rt, _ := strconv.Atoi(count["num"].(string))

	result.RecordsTotal = rt

	if data.Keyword != "" {
		db.StartGroup("AND")
		for _, v := range fields {
			db.WhereOr(v+" like", "%"+data.Keyword+"%")
		}
		db.EndGroup()

	}
	count, err = db.Row()
	if err != nil {
		return nil, err
	}
	rf, _ := strconv.Atoi(count["num"].(string))

	result.RecordsFiltered = rf

	db.Select("om.id,om.bukti_foto,om.tanggal,"+
		"om.qty,om.status,um.nama nama_user,"+
		"om.total+om.kode_unik total").
		Join("user_main um", "um.id = om.id_user", "")
	db.Limit(data.Length, data.Start)
	db.OrderBy(fields[data.OrdCol], data.OrdDir)
	dt, err := db.Result()
	if dt == nil {
		result.Data = []map[string]interface{}{}
	} else {
		result.Data = dt
	}
	return &result, nil
}

func OrderById(idOrder string, sel string) (map[string]interface{}, error) {
	var db = framework.Database{}
	defer db.Close()

	db.Select(sel).
		From("order_main om").
		Where("om.id", idOrder)
	return db.Row()
}

func OrderUpdate(idOrder string, update map[string]interface{}) error {
	var db = framework.Database{}
	defer db.Close()

	db.From("order_main").
		Where("id", idOrder)
	err :=  db.Update(update)
	return err
}

func OrderDetail(idOrder string) (map[string]interface{}, error) {
	var db = framework.Database{}
	defer db.Close()

	db.Select("om.id,om.tanggal,om.total,om.kode_unik, om.status, om.total+om.kode_unik jumlah_bayar,om.bukti_foto," +
		"om.metode, om.virtual_account,tp.logo logo_bank,tp.nama_bank,tp.nama_pembayaran,"+
		"em.nama nama_event, um.nama nama_user,um.email,um.no_hp, (om.total + om.kode_unik) total_harga,DATE_FORMAT(om.tanggal,'%W, %d %M %Y') tanggal_selesai ").
		From("order_main om").
		Join("tipe_pembayaran tp", "om.tipe_pembayaran = tp.id", "LEFT").
		Join("event_main em", "om.id_event = em.id", "").
		Join("user_main um", "um.id = om.id_user", "").
		Where("om.id", idOrder)
	data,err := db.Row()
	return data,err
}

func OrderHistory(idUser string) ([]map[string]interface{}, error) {
	var db = framework.Database{}
	defer db.Close()

	db.Select("o.id,o.tanggal,o.status,em.nama nama_event, "+
		"qty, (o.total + o.kode_unik) total_harga").
		From("order_main o").
		Join("event_main em", "o.id_event = em.id", "").
		Where("o.id_user", idUser)

	return db.Result()
}

func OrderByIdEventJumlah (idEvent string) (int,error){
	var db =framework.Database{}
	defer db.Close()

	db.Select("count(id) jml").
		From("order_main").
		Where("id_event",idEvent).
		Where("status","3")

	data,err := db.Row()
	if err != nil || data == nil {
		return 0, err
	}
	jml,_ := strconv.Atoi(data["jml"].(string))
	return jml,err

}

func OrderByIdEvent (idEvent string) ([]map[string]interface{},error){
	var db =framework.Database{}
	defer db.Close()

	db.Select("om.id,um.nama,om.tanggal, om. tanggal_selesai,om.total, om.kode_unik,tp.nama_pembayaran ," +
		"om.total+om.kode_unik total_keseluruhan, om.qty ").
		From("order_main om").
		Join("user_main um","om.id_user = um.id","").
		Join("tipe_pembayaran tp","om.tipe_pembayaran = tp.id","").
		Where("om.id_event",idEvent).
		Where("om.status","3")
	db.OrderBy("id","asc")
	db.GroupBy("om.id")

	data,err := db.Result()
	//fmt.Println(db.QueryView())
	return data,err
}

