package model

import (
	"cloud.google.com/go/translate"
	"context"
	"errors"
	"golang.org/x/text/language"

	languagee "cloud.google.com/go/language/apiv1"
	languagepb "google.golang.org/genproto/googleapis/cloud/language/v1"
)

func SentimenNL(tweet string) (int,error){
	ctx := context.Background()
	// Creates a client.
	client, err := languagee.NewClient(ctx)
	if err != nil {
		return 0,errors.New("Failed to analyze text: "+ err.Error())
	}
	// Sets the text to analyze.
	text := tweet

	// Detects the sentiment of the text.
	sentiment, err := client.AnalyzeSentiment(ctx, &languagepb.AnalyzeSentimentRequest{
		Document: &languagepb.Document{
			Source: &languagepb.Document_Content{
				Content: text,
			},
			Type: languagepb.Document_PLAIN_TEXT,
		},
		EncodingType: languagepb.EncodingType_UTF8,
	})
	if err != nil {
		return 0,errors.New("Failed to analyze text: "+ err.Error())
	}

	if sentiment.DocumentSentiment.Score >= 0 {
		return 1,nil //positive
	} else {
		return 0,nil //negative
	}

}

func Translate (text string )(string,error) {
	ctx := context.Background()
	// Creates a client.
	client, err := translate.NewClient(ctx)
	if err != nil {
		return "",errors.New("Failed to analyze text: "+ err.Error())
	}

	// Sets the text to translate.

	// Sets the target language.
	target, err := language.Parse("en")
	if err != nil {
		return "",errors.New("Failed to analyze text: "+ err.Error())
	}

	// Translates the text into Russian.
	translations, err := client.Translate(ctx, []string{text}, target, nil)
	if err != nil {
		return "",errors.New("Failed to analyze text: "+ err.Error())
	}

	return translations[0].Text,nil
}