FROM golang:alpine AS builder
RUN apk update && apk add --no-cache git
RUN mkdir /go/src/wahwah
WORKDIR /go/src/wahwah
COPY . .
RUN go get -u github.com/golang/dep/cmd/dep
RUN dep ensure
RUN go install

FROM alpine
RUN apk update && apk add ca-certificates && rm -rf /var/cache/apk/*
RUN apk add tzdata
RUN cp /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
RUN mkdir /app
WORKDIR /app
COPY --from=builder /go/bin/wahwah /app/
COPY --from=builder /go/src/wahwah/config.yaml /app/
COPY --from=builder /go/src/wahwah/pages /app/pages
COPY --from=builder /go/src/wahwah/asset /app/asset
EXPOSE 8080
ENTRYPOINT ./wahwah