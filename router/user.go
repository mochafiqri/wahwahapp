package router

import (
	"github.com/gin-gonic/gin"
	"wahwah/controller"
	"wahwah/middleware"
)

func user(r *gin.Engine) {
	rUser := r.Group("user")

	rUser.POST("/register", controller.DaftarUser)
	rUser.POST("/login", controller.LoginUser)

	rUser.POST("/event/list", controller.EventUser)
	rUser.POST("/event/detail", controller.EventDetailUser)

	rUser.POST("/homepage", controller.Homepage)
	rUser.POST("/homepage/banner", controller.HomepageBanner)
	rUser.POST("/homepage/band", controller.BandHomepage)
	rUser.POST("/homepage/event", controller.EventHomepage)
	rUser.POST("/homepage/tiket", controller.TiketHomepage)

	rUser.POST("/tipe-pembayaran", controller.TipePembayaranList)

	rUser.Use(middleware.AuthUser)
	rUser.POST("/checkout", controller.Checkout)
	rUser.POST("/order/detail-order", controller.OrderDetail)
	rUser.POST("/order/konfirmasi-pembayaran", controller.OrderKonfirmasiPembayaran)
	rUser.POST("/order", controller.OrderHistory)
	rUser.POST("/order/pembayaran-midtrans", controller.PembayaranMidtrans)

	rUser.POST("/my-tiket", controller.MyTiket)
	rUser.POST("/my-tiket/detail", controller.MyTiketDetail)
	rUser.POST("/my-tiket/refund", controller.MyTiketRefund)

	rUser.POST("/my-tiket/refund/list", controller.MyTiketRefundList)

	rUser.POST("/review/add", controller.ReviewAdd)

	rUser.POST("/profile", controller.Profil)

}
