package router

import (
	"github.com/gin-gonic/gin"
	"wahwah/controller"
)

func admin (r *gin.Engine){
	rAdmin := r.Group("api/admin")
	rAdmin.POST("/login",controller.LoginProses)
	rAdmin.GET("/logout", controller.Logout)



	rAdmin.POST("/tiket/detail",controller.MyTiketDetail)
	rAdmin.POST("/tiket/check-in",controller.TiketCheckin)

	rAdmin.POST("/midtrans/charge",controller.ChargeMidtrans)
	rAdmin.POST("/midtrans/notif",controller.NotificationMidtrans)

	rAdmin.GET("/export/order",controller.OrderExport)
	rAdmin.GET("/export/tiket",controller.TiketExport)
	rAdmin.GET("/export/keuangan",controller.KeuanganExport)


	//rAdmin.Use(middleware.AuthAPI)
	rAdmin.POST("/admin/datatable",controller.Admindatatable)
	rAdmin.POST("/admin/create",controller.AdminAdd)


	rAdmin.POST("/band/datatable",controller.Banddatatable)
	rAdmin.POST("/band/create",controller.BandCreate)
	rAdmin.POST("/band/getById",controller.BandGetById)
	rAdmin.POST("/band/update",controller.BandUpdate)
	rAdmin.POST("/band/publish",controller.BandPublish)

	rAdmin.POST("/event/create",controller.EventTambah)
	rAdmin.POST("/event/datatable",controller.Eventdatatable)
	rAdmin.POST("/event/getById",controller.EventGetById)
	rAdmin.POST("/event/basic-update",controller.EventBasicUpdate)
	rAdmin.POST("/event/set-performer",controller.SetPerformer)
	rAdmin.POST("/event/performer-list",controller.EevntBanddatatable)
	rAdmin.POST("/event/refundable",controller.SetRefundable)
	rAdmin.POST("/event/selesaikan",controller.EventSelesai)
	rAdmin.POST("/event/set-status",controller.EventSetStatus)
	rAdmin.POST("/event/set-rundown",controller.BandSetRundown)

	rAdmin.POST("/event-poster/add",controller.AddPoster)

	rAdmin.POST("/event-admin/add",controller.EventAdminAdd)

	rAdmin.POST("/event-kategori/create",controller.TiketKategoriAdd)
	rAdmin.POST("/event-kategori/get",controller.TiketKategoriGet)

	rAdmin.POST("/info-event/create",controller.InfoAdd)
	rAdmin.POST("/info-event/getById",controller.InfoGetById)

	rAdmin.POST("/tiket/create",controller.TiketAdd)
	rAdmin.POST("/tiket/sold",controller.TiketSold)
	rAdmin.POST("/tiket/getById",controller.TiketGetById)
	rAdmin.POST("/tiket-kategori/list",controller.TiketKategoriList)
	rAdmin.POST("/tiket-kategori/sold",controller.SetTiketKategoriSold)

	rAdmin.POST("/banner/create",controller.BannerAdd)


	rAdmin.POST("/order/pending",controller.OrderPending)
	rAdmin.POST("/order/list-cancel",controller.OrderCancelList)
	rAdmin.POST("/order/list-sukses",controller.OrderSuksesList)
	rAdmin.POST("/order/detail",controller.OrderDetail)
	rAdmin.POST("/order/cancel",controller.OrderCancel)
	rAdmin.POST("/order/confirm",controller.OrderConfirm)

	rAdmin.POST("/sentimen/get",controller.Sentimen)

	rAdmin.POST("/catatan-keuangan/get-dtb",controller.CatatanKeuanganEvent)
	rAdmin.POST("/catatan-keuangan/add",controller.AddCatatanKeuangan)

	rAdmin.POST("/catatan-keuangan-kategori/add",controller.CatatanKeuanganKategoriAdd)
	rAdmin.POST("/catatan-keuangan-kategori/get",controller.CatatanKeuanganKategoriGet)

	rAdmin.POST("/review/list",controller.Reviewdatatable)

	rAdmin.POST("/notification/add",controller.NotificationAdd)

	rAdmin.POST("/request-refund/list",controller.RefundPendingDataTable)
	rAdmin.POST("/refund/list",controller.RefundDataTable)
	rAdmin.POST("/request-refund/konfirm",controller.RefundConfirm)
	rAdmin.POST("/request-refund/cancel",controller.RefundCancel)

	rAdmin.POST("/last-checkin",controller.LastCheckin)




}
