package router

import (
	"github.com/gin-gonic/gin"
	"wahwah/controller"
	"wahwah/middleware"
)



func pages (r *gin.Engine) {
	rAdmin := r.Group("")
	rAdmin.GET("/login", controller.LoginAdmin)
	rAdmin.GET("/logout", controller.Logout)

	rAdmin.Use(middleware.Auth)
	rAdmin.GET("/", controller.Dashboard)
	rAdmin.GET("/band", controller.BandPage)
	rAdmin.GET("/admin", controller.AdminPage)
	rAdmin.GET("/event", controller.EventPage)
	rAdmin.GET("/event/:id_event", controller.EventSettingPage)
	rAdmin.GET("/catatan-keuangan/:id_event", controller.CatatanUang)
	rAdmin.GET("/event-poster/:id_event", controller.EventPoster)
	rAdmin.GET("/review/:id_event", controller.EventReview)

	rAdmin.GET("/sentimen", controller.AnalyticPerformer)
	rAdmin.GET("/order-pending", controller.OrderPendingPage)
	rAdmin.GET("/order-success", controller.OrderSuksesPage)
	rAdmin.GET("/order-cancel", controller.OrderCancelPage)

	rAdmin.GET("/tiket-refund", controller.TiketRefundPage)

	rAdmin.GET("/notification", controller.Notification)

	rAdmin.GET("/laporan-event/:id_event", controller.LaporaEvent)



}
