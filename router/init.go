package router

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"wahwah/controller"
)

func Init(r *gin.Engine) {
	r.Static("/asset", "./asset")
	r.Static("/public", "./public")
	r.LoadHTMLGlob("./pages/**/*")

	r.NoRoute(error404)
	r.NoMethod(error404)
	r.GET("/check_service", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"code": "200",
			"msg":  "Service running",
			"data": gin.H{},
		})
	})
	r.GET("/check_twwet",controller.Tweet)
	r.GET("/check_google",controller.NaturalTest)
	r.GET("/translate",controller.Translate)
	r.GET("/tes",controller.TestFloat)
	r.GET("/privacy-and-policy",controller.PrivacyPolicy)
	r.GET("/qr/:kode",controller.Qr)


	admin(r)
	user(r)
	pages(r)
}

func error404(c *gin.Context) {
	c.HTML(http.StatusOK,"template/404",gin.H{
		"title" : "Halaman Tidak Ditemukan",
	})
}

