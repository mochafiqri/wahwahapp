package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"wahwah/lib"
	"wahwah/model"
)

func InfoAdd (c *gin.Context){
	idEvent := c.PostForm("id_event")
	txtInfo := c.PostForm("info")
	notif := c.PostForm("notif")
	//banner,err := c.FormFile("banner")
	//if err != nil {
	//	lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
	//	return
	//}

	var insert = map[string]interface{}{
		"id_event" : idEvent,
		"info" : txtInfo,
	}
	err := model.TambahInfo(insert)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if notif == "1"{
		//Kirim notif
	}

	lib.JSON(c,http.StatusOK,"Sukses Tambah Info",gin.H{})
}

func InfoGetById(c *gin.Context){
	idInfo := c.PostForm("id_info")
	data,err := model.GetInfoById(idInfo)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if data == nil {
		lib.JSON(c,http.StatusNotFound,"Info Tidak di Temukan",gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Sukses Tambah Info",data)

}