package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
	"wahwah/lib"
	"wahwah/model"
)

func BandPage (c *gin.Context){
	js := []string{
		"/asset/plugins/jquery-countto/jquery.countTo",
		"/asset/plugins/jquery-datatable/jquery.dataTables",
		"/asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap",
		"/asset/plugins/jquery-datatable/extensions/export/dataTables.buttons.min",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.flash.min",
		"/asset/plugins/jquery-datatable/extensions/export/jszip.min",
		"/asset/plugins/jquery-datatable/extensions/export/pdfmake.min",
		"/asset/plugins/jquery-datatable/extensions/export/vfs_fonts",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.html5.min",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.print.min",
		"/asset/plugins/jquery-validation/jquery.validate",
		"/asset/js/main/band",

		//"asset/js/pages/forms/basic-form-elements",
	}
	css := []string{
		"asset/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap",
		"/asset/plugins/waitme/waitMe",
		"/asset/plugins/bootstrap-select/css/bootstrap-select",
	}
	c.HTML(http.StatusOK,"band/index",gin.H{
		"title" : "Band Admin",
		"js" : js,
		"css" : css,
	})
}

func Banddatatable (c *gin.Context){
	var data model.DatatablesSource
	err := c.Bind(&data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	result,err := model.Banddatatable(data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	c.JSON(http.StatusOK,&result)

}

func BandCreate(c *gin.Context){
	var data = model.ModelBand{}
	err := c.Bind(&data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}


	var insert = map[string]interface{}{
		"nama" :data.Nama,
		"deskripsi" :data.Deskripsi,
		"facebook" :data.Facebook,
		"twitter" :data.Twitter,
		"instagram" :data.Instagram,
		"youtube" :data.Youtube,
	}

	id,err := model.BandSave(insert)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	file, err := c.FormFile("file")
	if file == nil && file.Filename == "" {
		lib.JSON(c, http.StatusInternalServerError, "Error File", gin.H{})
		return
	}
	tmp := file.Filename
	tmp1 := strings.Split(tmp, ".")
	tmp1[0] = tmp1[0]
	name := strings.Join(tmp1, ".")

	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	path := "./public/img/band/"
	pathPoster := path+name

	err = lib.Resize(file,750,0,pathPoster)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	var insertFoto = map[string]interface{}{
		"id_band" : id,
		"no" : 1,
		"foto" : pathPoster,
		"main" : 1,
		"publish" : 1,
	}
	_,err = model.AddFotoMain(insertFoto)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Sukses Membuat Data Band",insert)
	return
}

func BandGetById (c *gin.Context){
	id := c.PostForm("id")
	data,err := model.BandGetById(id,"id,nama,deskripsi,facebook,twitter,instagram,youtube")
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if len(data) == 0 || id == "" {
		lib.JSON(c,http.StatusNotFound,"Data Tidak di Temukan",gin.H{})
		return
	}

	lib.JSON(c,http.StatusOK,"Data Band",data)
	return

}

func BandUpdate (c *gin.Context){
	var data = model.ModelBand{}
	err := c.Bind(&data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	var update = map[string]interface{}{
		"nama" :data.Nama,
		"deskripsi" :data.Deskripsi,
		"facebook" :data.Facebook,
		"twitter" :data.Twitter,
		"instagram" :data.Instagram,
		"youtube" :data.Youtube,
		"keyword" : data.Nama,
	}
	err = model.BandUpdate(data.Id,update)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Sukses Update Data Band",update)
	return
}

func TambahFotoMain(c *gin.Context){
	//idBand := c.PostForm("id_band")
	//foto,err := c.FormFile("foto_main")
	//var name string
	//if err != nil {
	//	lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
	//	return
	//}
	//data, err := model.BandGetById(idBand, "id")
	//if err != nil {
	//	lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
	//	return
	//}
	//if data == nil {
	//	lib.JSON(c,http.StatusInternalServerError,"Terjadi kesalahan coba lagi",gin.H{})
	//	return
	//}
	//if foto != nil  && foto.Filename != "" {
	//	tmp := foto.Filename
	//	path := "./public/img/fotoKaryawan/"
	//	name = path+tmp
	//	if err := c.SaveUploadedFile(file, name); err != nil {
	//		framework.ErrorJson(err.Error(), c)
	//		return
	//	}
	//}else{
	//	lib.JSON(c,http.StatusInternalServerError,"Terjadi kesalahan coba lagi",gin.H{})
	//	return
	//}
	//
}


func BandPublish (c *gin.Context){
	idBand := c.PostForm("id")
	data,err := model.BandGetById(idBand,"id,publish")
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if data == nil {
		lib.JSON(c,http.StatusNotFound,"Data Band tidak Ditemuka",gin.H{})
		return
	}
	var publish = data["publish"].(string)
	var update = map[string]interface{}{}
	var msg string
	if publish == "1"{
		update["publish"] = 0
		msg = "Berhasil Unpublish Band"
	}else{
		update["publish"] = 1
		msg = "Berhasil Publish Band"

	}
	err = model.BandUpdate(idBand,update)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	lib.JSON(c,http.StatusOK,msg,gin.H{})
	return

}

func BandHomepage(c *gin.Context)  {
	data,err := model.BandGetHomepage()
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Band OK",data)
	return

}
func BandSetRundown(c *gin.Context){
	var idEventBand = c.PostForm("id_event_band")
	var time = c.PostForm("wkt_mulai")

	err := model.BandEventUpdateRundown(idEventBand,time)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Update Rundown Success",gin.H{})
	return


}


