package controller

import (
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/bandros/framework"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"
	"wahwah/lib"
	"wahwah/model"
)

func Checkout(c *gin.Context) {
	var user = c.MustGet("jwt").(jwt.MapClaims)
	idUser := user["id"].(string)
	var idTiketKategori = c.PostForm("tiket_kategori")
	var qtyString= c.PostForm("qty")

	idTiketKategoriArray := strings.Split(idTiketKategori,",")
	qtyArray := strings.Split(qtyString,",")

	if len(idTiketKategori) != len(qtyString)  || len(qtyString) == 0 {
		lib.JSON(c, http.StatusBadRequest, "Jumlah tiket tidak boleh kosong", gin.H{})
		return
	}
	var total = 0
	var total_qty = 0
	var id_event string = ""
	for i,v := range idTiketKategoriArray{
		qty,_ := strconv.Atoi(qtyArray[i])
		//if qty > 5 {
		//	lib.JSON(c, http.StatusBadRequest, "Maksimal Pembeliann 5", gin.H{})
		//	return
		//}
		//var total = c.PostForm("total")
		tiketK, err := model.TiketKategoriGetById(v)
		if err != nil {
			lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
			return
		}
		if tiketK == nil || idTiketKategori == "" || v == "" {
			lib.JSON(c, http.StatusNotFound, "Tiket Event Tidak Tersedia", gin.H{})
			return
		}
		id_event = tiketK["id_event"].(string)

		var jmlTiket int
		datajmlTiket, err := model.TiketKetersediaan(v)

		if datajmlTiket == nil {
			jmlTiket = 0
		} else {
			jmlTiket, _ = strconv.Atoi(datajmlTiket["jml"].(string))
		}
		if jmlTiket < qty {
			lib.JSON(c, http.StatusBadRequest, "Tiket sudah tidak "+tiketK["nama"].(string)+"tersedia / kurang", gin.H{})
			return
		}
		harga_tiket, _ := strconv.Atoi(tiketK["harga"].(string))
		total_perTiket := harga_tiket * qty
		total = total + total_perTiket
		total_qty = total_qty + qty
	}

	kodeUnik := lib.KodeUnik()
	var db = framework.Database{}
	defer db.Close()

	db.Transaction()
	var insert = []string{
		idUser,
		strconv.Itoa(total),
		strconv.Itoa(kodeUnik),
		strconv.Itoa(total_qty),
		id_event,
	}
	idOrder, err := model.InsertOrder(db,insert)

	if err != nil {
		db.Rollback()
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	for i,v := range idTiketKategoriArray {
		qty,_ := strconv.Atoi(qtyArray[i])
		err = model.TiketSetOrder(db,v, qty, idOrder["id"].(string))
		if err != nil {
			db.Rollback()
			lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
			return
		}
	}
	_,err = model.TiketByIdOrder(idOrder["id"].(string))
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	db.Commit()
	lib.JSON(c, http.StatusOK, "Sukses Order", idOrder)
}



func OrderKonfirmasiPembayaran(c *gin.Context) {
	idOrder := c.PostForm("id_order")
	tmpOrder, err := model.OrderById(idOrder, "id")
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	if tmpOrder == nil || idOrder == "" {
		lib.JSON(c, http.StatusNotFound, "Data tidak ditemukan", gin.H{})
		return
	}
	file, err := c.FormFile("foto")
	if file == nil && file.Filename == "" {
		lib.JSON(c, http.StatusInternalServerError, "Error File", gin.H{})
		return
	}
	tmp := file.Filename
	tmp1 := strings.Split(tmp, ".")
	tmp1[0] = tmp1[0] + "-" + idOrder
	name := strings.Join(tmp1, ".")

	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	path := "./public/img/bukti_konfirmasi/"
	pathPoster := path+name
	link := "http://wahwah.my.id:8080/public/img/bukti_konfirmasi/"+name
	err = lib.Resize(file,500,0,pathPoster)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	//if err := c.SaveUploadedFile(file, pathPoster); err != nil {
	//	if err != nil {
	//		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
	//		return
	//	}
	//}
	var update = map[string]interface{}{
		"bukti_foto": link,
		"status":     2,
	}
	err = model.OrderUpdate(idOrder, update)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	lib.JSON(c, http.StatusOK, "Berhasil Konfirmasi", gin.H{})
	return
}

func OrderPending(c *gin.Context) {
	var data model.DatatablesSource
	err := c.Bind(&data)

	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	result, err := model.OrderPendingDtb(data)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	c.JSON(http.StatusOK, &result)
}

func OrderSuksesList(c *gin.Context) {
	var data model.DatatablesSource
	err := c.Bind(&data)

	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	result, err := model.OrderSuksesDtb(data)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	c.JSON(http.StatusOK, &result)
}

func OrderCancelList(c *gin.Context) {
	var data model.DatatablesSource
	err := c.Bind(&data)

	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	result, err := model.OrderCancelDtb(data)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	c.JSON(http.StatusOK, &result)
}

func OrderDetail(c *gin.Context) {
	var idOrder = c.PostForm("id_order")

	data, err := model.OrderDetail(idOrder)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	if data == nil || idOrder == "" {
		lib.JSON(c, http.StatusNotFound, "Order tidak ditemukan", gin.H{})
		return
	}

	dataTiket,err := model.GetTiketByIdOrder(idOrder)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	dataTiketRefund,err := model.GetTiketRefundByIdOrder(idOrder)
	data["tiket_main"] = dataTiket
	data["tiket_refund"] = dataTiketRefund
	lib.JSON(c, http.StatusOK, "Order Detail", data)
}

func OrderConfirm(c *gin.Context) {
	var idOrder = c.PostForm("id_order")
	data, err := model.OrderById(idOrder, "id,id_event,total,kode_unik")
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	if data == nil || idOrder == "" {
		lib.JSON(c, http.StatusNotFound, "Order tidak ditemukan", gin.H{})
		return
	}
	var now = time.Now()
	var tanggal = now.Format("2006-01-02 15:04:05")

	var update = map[string]interface{}{
		"status":          3,
		"tanggal_selesai": tanggal,
	}

	err = model.OrderUpdate(idOrder, update)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}

	err = model.TiketUpdateConfirm(idOrder)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}

	//Membuat keuangan penjualan tiket
	err = AddConfirmByOrder(idOrder)

	lib.JSON(c, http.StatusOK, "Berhasil Konfirmasi", gin.H{})
}

func OrderPendingPage(c *gin.Context) {
	js := []string{
		"/asset/plugins/jquery-countto/jquery.countTo",
		"/asset/plugins/jquery-datatable/jquery.dataTables",
		"/asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap",
		"/asset/plugins/jquery-datatable/extensions/export/dataTables.buttons.min",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.flash.min",
		"/asset/plugins/jquery-datatable/extensions/export/jszip.min",
		"/asset/plugins/jquery-datatable/extensions/export/pdfmake.min",
		"/asset/plugins/jquery-datatable/extensions/export/vfs_fonts",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.html5.min",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.print.min",
		"/asset/plugins/jquery-validation/jquery.validate",
		"/asset/plugins/light-gallery/js/lightgallery",
		"/asset/js/main/order-pending",
	}
	css := []string{
		"asset/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap",
		"/asset/plugins/waitme/waitMe",
		"/asset/plugins/bootstrap-select/css/bootstrap-select",
		"/asset/plugins/light-gallery/css/lightgallery",
	}
	c.HTML(http.StatusOK, "order-pending/index", gin.H{
		"title": "Order Pending Admin",
		"js":    js,
		"css":   css,
	})
}

func OrderSuksesPage(c *gin.Context) {
	js := []string{
		"/asset/plugins/jquery-countto/jquery.countTo",
		"/asset/plugins/jquery-datatable/jquery.dataTables",
		"/asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap",
		"/asset/plugins/jquery-datatable/extensions/export/dataTables.buttons.min",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.flash.min",
		"/asset/plugins/jquery-datatable/extensions/export/jszip.min",
		"/asset/plugins/jquery-datatable/extensions/export/pdfmake.min",
		"/asset/plugins/jquery-datatable/extensions/export/vfs_fonts",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.html5.min",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.print.min",
		"/asset/plugins/jquery-validation/jquery.validate",
		"/asset/plugins/light-gallery/js/lightgallery",
		"/asset/js/main/order-sukses",
	}
	css := []string{
		"asset/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap",
		"/asset/plugins/waitme/waitMe",
		"/asset/plugins/bootstrap-select/css/bootstrap-select",
		"/asset/plugins/light-gallery/css/lightgallery",
	}
	c.HTML(http.StatusOK, "order-sukses/index", gin.H{
		"title": "Order Pending Admin",
		"js":    js,
		"css":   css,
	})
}

func OrderCancelPage(c *gin.Context) {
	js := []string{
		"/asset/plugins/jquery-countto/jquery.countTo",
		"/asset/plugins/jquery-datatable/jquery.dataTables",
		"/asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap",
		"/asset/plugins/jquery-datatable/extensions/export/dataTables.buttons.min",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.flash.min",
		"/asset/plugins/jquery-datatable/extensions/export/jszip.min",
		"/asset/plugins/jquery-datatable/extensions/export/pdfmake.min",
		"/asset/plugins/jquery-datatable/extensions/export/vfs_fonts",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.html5.min",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.print.min",
		"/asset/plugins/jquery-validation/jquery.validate",
		"/asset/plugins/light-gallery/js/lightgallery",
		"/asset/js/main/order-cancel",
	}
	css := []string{
		"asset/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap",
		"/asset/plugins/waitme/waitMe",
		"/asset/plugins/bootstrap-select/css/bootstrap-select",
		"/asset/plugins/light-gallery/css/lightgallery",
	}
	c.HTML(http.StatusOK, "order-cancel/index", gin.H{
		"title": "Order Pending Admin",
		"js":    js,
		"css":   css,
	})
}

func OrderCancel(c *gin.Context){
	var idOrder = c.PostForm("id_order")
	dataOrder,err := model.OrderById(idOrder,"id,status")
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	if dataOrder == nil || idOrder == ""{
		lib.JSON(c, http.StatusNotFound, "Id Order tidak ditemukan", gin.H{})
		return
	}
	status_order,_ := strconv.Atoi(dataOrder["status"].(string))
	if status_order > 1 {
		lib.JSON(c, http.StatusBadRequest, "Id Order tidak di batalkan", gin.H{})
		return
	}
	var update = map[string]interface{}{
		"status" : 3,
	}
	err = model.OrderUpdate(idOrder,update)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}

	err = model.TiketCancelByIdOrder(idOrder)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}

	lib.JSON(c, http.StatusOK, "Order Berhasil di batalkann", gin.H{})

}

func OrderHistory(c *gin.Context) {
	var user = c.MustGet("jwt").(jwt.MapClaims)
	idUser := user["id"].(string)

	data, err := model.OrderHistory(idUser)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	lib.JSON(c, http.StatusOK, "Data Order History", data)
}

func OrderExport (c *gin.Context){
	var idEvent = c.Query("id_event")

	data,err := model.OrderByIdEvent(idEvent)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	xlsx := excelize.NewFile()
	sheet1Name := "Sheet One"
	xlsx.SetSheetName(xlsx.GetSheetName(1), sheet1Name)
	xlsx.MergeCell(sheet1Name, "A1", "E1")
	xlsx.SetCellValue(sheet1Name, "A1", "Order Event")

	xlsx.SetCellValue(sheet1Name, "A3", "ID ORDER")
	xlsx.SetCellValue(sheet1Name, "B3", "NAMA")
	xlsx.SetCellValue(sheet1Name, "C3", "TANGGAL")
	xlsx.SetCellValue(sheet1Name, "D3", "QTY")
	xlsx.SetCellValue(sheet1Name, "E3", "HARGA")
	xlsx.SetCellValue(sheet1Name, "F3", "KODE UNIK")
	xlsx.SetCellValue(sheet1Name, "G3", "TOTAL")
	xlsx.SetCellValue(sheet1Name, "H3", "PEMBAYARAN")

	style, err := xlsx.NewStyle(`{
    "font": {go
        "bold": true,
        "size": 12
    }
	}`)

	err1 := xlsx.AutoFilter(sheet1Name, "A3", "E3", "")
	xlsx.SetCellStyle(sheet1Name, "A3", "E3", style)

	if err1 != nil {
		log.Fatal("ERROR", err1.Error())
	}

	for i, each := range data {
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("A%d", i+4), each["id"])
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("B%d", i+4), each["nama"])
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("C%d", i+4), each["tanggal"])
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("D%d", i+4), each["qty"])
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("E%d", i+4), each["total"])
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("F%d", i+4), each["kode_unik"])
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("G%d", i+4), each["total_keseluruhan"])
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("H%d", i+4), each["nama_pembayaran"])
	}
	c.Writer.Header().Set("Content-Type", "application/octet-stream")
	c.Writer.Header().Set("Content-Disposition", "attachment; filename="+"LaporanOrder.xlsx")
	c.Writer.Header().Set("Content-Transfer-Encoding", "binary")
	c.Writer.Header().Set("Expires", "0")
	err = xlsx.Write(c.Writer)

	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}


}
