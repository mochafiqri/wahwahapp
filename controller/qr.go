package controller

import (
	"github.com/boombuler/barcode"
	"github.com/boombuler/barcode/qr"
	"github.com/gin-gonic/gin"
	"image/png"
)

func Qr (c *gin.Context){
	string := c.Param("kode")
	qrCode, _ := qr.Encode(string, qr.L, qr.Auto)
	qrCode, _ = barcode.Scale(qrCode, 512, 512)

	png.Encode(c.Writer, qrCode)

}
