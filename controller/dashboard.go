package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"wahwah/lib"
	"wahwah/model"
)

func Dashboard (c *gin.Context){
	js := []string{
		"/asset/js/dashboard",
		"/asset/plugins/jquery-countto/jquery.countTo",
		"/asset/plugins/raphael/raphael.min",
		"/asset/plugins/morrisjs/morris",
		"/asset/plugins/chartjs/Chart.bundle",
		"/asset/plugins/flot-charts/jquery.flot",
		"/asset/plugins/flot-charts/jquery.resize",
		"/asset/plugins/flot-charts/jquery.pie",
		"/asset/plugins/flot-charts/jquery.categories",
		"/asset/plugins/flot-charts/jquery.time",
		"/asset/plugins/jquery-sparkline/jquery.sparkline",
	}
	css := []string{
		"/asset/plugins/morrisjs/morris",
	}
	data,err := model.GetEventDashboard()
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	var result = []map[string]interface{}{}
	var status string
	var pb int
	var color string
	for _,v := range data {
		if v["status"].(string) == "1" {
			status = "Planning"
			pb = 15
			color = "red"
		} else if v["status"].(string) == "2" {
			status = "Pre Day (Ticketing)"
			pb = 40
			color = "yellow"
		}else if v["status"].(string) == "3" {
			status = "The Day"
			pb = 75
			color = "green"
		}else if v["status"].(string) == "4" {
			pb = 90
			status = "Rate Time"
			color = "blue"
		}else if v["status"].(string) == "5" {
			status = "Done"
			pb = 100
			color = "light-blue"
		}
		result = append(result,map[string]interface{}{
			"nama_tempat" : v["nama_tempat"],
			"id" : v["id"],
			"nama" : v["nama"],
			"status" : status,
			"pb" : pb,
			"color" : color,
		})
	}


	c.HTML(http.StatusOK,"dashboard/index",gin.H{
		"title" : "Dashboard",
		"js" : js,
		"css" : css,
		"data" : result,
	})
}
