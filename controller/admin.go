package controller

import (
	"github.com/bandros/framework"
	"github.com/gin-gonic/gin"
	"net/http"
	"wahwah/lib"
	"wahwah/model"
)

func PrivacyPolicy (c *gin.Context){
	c.HTML(http.StatusOK, "privacy-policy/index", gin.H{
		"title": "Privacy Policy Wah Wah App",
	})}
func AdminPage (c *gin.Context){
	js := []string{
		"/asset/plugins/jquery-countto/jquery.countTo",
		"/asset/plugins/jquery-datatable/jquery.dataTables",
		"/asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap",
		"/asset/plugins/jquery-datatable/extensions/export/dataTables.buttons.min",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.flash.min",
		"/asset/plugins/jquery-datatable/extensions/export/jszip.min",
		"/asset/plugins/jquery-datatable/extensions/export/pdfmake.min",
		"/asset/plugins/jquery-datatable/extensions/export/vfs_fonts",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.html5.min",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.print.min",
		"/asset/plugins/jquery-validation/jquery.validate",
		"/asset/js/main/admin",

		//"asset/js/pages/forms/basic-form-elements",
	}
	css := []string{
		"asset/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap",
		"/asset/plugins/waitme/waitMe",
		"/asset/plugins/bootstrap-select/css/bootstrap-select",
	}
	c.HTML(http.StatusOK,"admin/index",gin.H{
		"title" : " Admin",
		"js" : js,
		"css" : css,
	})
}

func Admindatatable (c *gin.Context){
	var data model.DatatablesSource
	err := c.Bind(&data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	result,err := model.Admindatatable(data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	c.JSON(http.StatusOK,&result)

}

func AdminAdd (c *gin.Context){
	var nama = c.PostForm("nama")
	var username = c.PostForm("username")
	var email = c.PostForm("email")
	var password = c.PostForm("password")

	var insert = map[string]interface{}{
		"nama" : nama,
		"username" : username,
		"email" : email,
		"password" : framework.Password(password),
	}

	_,err := model.AdminAdd(insert)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Sukses Tambah Admin",gin.H{})


}