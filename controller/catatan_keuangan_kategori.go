package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"wahwah/lib"
	"wahwah/model"
)

func CatatanKeuanganKategoriAdd (c *gin.Context){
	var nama = c.PostForm("nama")
	var tipe = c.PostForm("type")

	if nama == "" || tipe == "" {
			lib.JSON(c,http.StatusBadRequest,"Nama ata tipe wajib diisi",gin.H{})
			return
	}
	var insert = map[string]interface{}{
		"nama" : nama,
		"type" : tipe,
	}
	_,err := model.CatatanKeuanganKategoriAdd(insert)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	fmt.Println(err)
	lib.JSON(c,http.StatusOK,"Berhasil Tambah Kategori",gin.H{})
	return

}

func CatatanKeuanganKategoriGet (c *gin.Context){
	//idEvent := c.PostForm("id_event")

	//data,err := model.CatatanKeuanganKategoriGet(idEvent)
	//if err != nil {
	//	lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
	//	return
	//}
	//lib.JSON(c,http.StatusOK,"Catatan Keuangan Kategori",data)
	//return
}