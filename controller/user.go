package controller

import (
	"github.com/bandros/framework"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"wahwah/lib"
	"wahwah/model"
)

func LoginUser (c *gin.Context){
	user := c.PostForm("user")
	password := c.PostForm("password")

	dataUser,err := model.UserGet(user)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if dataUser == nil {
		lib.JSON(c,http.StatusInternalServerError,"Maaf, User Tidak Terdaftar",gin.H{})
		return
	}


	valid :=framework.ValidPassword(password,dataUser["password"].(string))

	if valid == false {
		lib.JSON(c,http.StatusInternalServerError,"Password Salah",gin.H{})
		return
	}

	token,err := lib.GenerateTokenUser(dataUser)
	if err != nil {
		lib.JSON(c, http.StatusNotFound, err.Error(), gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Berhasil Login",gin.H{
		"token" : token,
	})
	return



}

func DaftarUser (c *gin.Context){
	var UserModel = model.UserModel{}
	err := c.Bind(&UserModel)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	var number = framework.NumberPhone(UserModel.Hp)
	res,err := model.CheckNumber(number, UserModel.Email)
	if err != nil {
		lib.JSON(c, http.StatusNotFound, err.Error(), gin.H{})
		return
	}
	if res != nil {
		lib.JSON(c, http.StatusInternalServerError,"No Hp / Email Sudah Terdaftar", gin.H{})
		return
	}
	UserModel.Hp = number
	data,err := model.Register(UserModel)
	if err != nil {
		lib.JSON(c, http.StatusNotFound, err.Error(), gin.H{})
		return
	}

	token,err := lib.GenerateTokenUser(data)
	if err != nil {
		lib.JSON(c, http.StatusNotFound, err.Error(), gin.H{})
		return
	}

	lib.JSON(c,http.StatusOK,"Sukses Daftar",gin.H{
		"token" : token,
	})
	return
}

func HomepageBanner (c *gin.Context){
	banner,err := model.BannerGet("*","no","asc")
	if err != nil {
		lib.JSON(c, http.StatusNotFound, err.Error(), gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Homepage",banner)
	return
}

func Profil (c *gin.Context){
	var user = c.MustGet("jwt").(jwt.MapClaims)
	idUser := user["id"].(string)

	data,err := model.UserById(idUser,"*")
	if err != nil {
		lib.JSON(c, http.StatusNotFound, err.Error(), gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Homepage",data)
	return
}

func Homepage (c *gin.Context){
	var result =  map[string]interface{}{}
	banner,err := model.BannerGet("*","no","asc")
	if err != nil {
		lib.JSON(c, http.StatusNotFound, err.Error(), gin.H{})
		return
	}
	band,err := model.BandGetHomepage()
	if err != nil {
		lib.JSON(c, http.StatusNotFound, err.Error(), gin.H{})
		return
	}

	event,err := model.GetEventHomepage()
	if err != nil {
		lib.JSON(c, http.StatusNotFound, err.Error(), gin.H{})
		return
	}

	result["banner"] = banner
	result["band"] = band
	result["event"] = event
	lib.JSON(c,http.StatusOK,"Homepage",result)
	return
}