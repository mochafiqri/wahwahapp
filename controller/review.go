package controller

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"wahwah/lib"
	"wahwah/model"
)

func EventReview (c *gin.Context){

	js := []string{
		"/asset/plugins/momentjs/moment",
		"/asset/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker",
		"/asset/plugins/jquery-datatable/jquery.dataTables",
		"/asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap",
		"/asset/js/main/review",
	}

	css := []string{
		"asset/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap",
		"/asset/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker",
		"/asset/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap",
		"/asset/plugins/waitme/waitMe",
		"/asset/plugins/bootstrap-select/css/bootstrap-select",

	}

	idEvent := c.Param("id_event")

	event,err := model.EventGetById(idEvent,"id,nama")
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if event == nil {
		c.HTML(http.StatusOK,"template/404",gin.H{
			"title" : "Halaman Tidak Ditemukan",
		})
	}


	c.HTML(http.StatusOK,"review-event/index",gin.H{
		"title" : "Review Event",
		"js" : js,
		"css" : css,
		"event" : event,
	})

}

func ReviewAdd(c *gin.Context){
	var user = c.MustGet("jwt").(jwt.MapClaims)
	idUser := user["id"].(string)
	idEvent := c.PostForm("id_event")
	rating := c.PostForm("rating")
	review := c.PostForm("review")

	if idEvent == "" {
		lib.JSON(c,http.StatusInternalServerError,"Id event wajib diisi",gin.H{})
		return
	}
	dataEvent,err := model.EventGetById(idEvent,"status")
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if dataEvent["status"].(string) == "3"{
		var updateEvent = map[string]interface{}{
			"status" : 4,
		}
		_ = model.EventUpdate(idEvent,updateEvent)
	}
	cek,err := model.CheckReview(idEvent,idUser)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	if cek == nil {
		var insert = map[string]interface{}{
			"id_event" : idEvent,
			"rating" : rating,
			"review" : review,
			"id_user" : idUser,
		}

		_,err = model.ReviewAdd(insert)
		if err != nil {
			lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
			return
		}
	}else{
		var update = map[string]interface{}{
			"id_event" : idEvent,
			"rating" : rating,
			"review" : review,
			"id_user" : idUser,
		}
		err = model.UpdateReview(cek["id"].(string),update)
		if err != nil {
			lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
			return
		}
	}

	lib.JSON(c,http.StatusOK,"Terimakasih telah memberi kami feedback ",gin.H{})
	return

}
func Reviewdatatable (c *gin.Context){
	var data model.DatatablesSource
	err := c.Bind(&data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	result,err := model.Reviewdatatable(data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	c.JSON(http.StatusOK,&result)
}