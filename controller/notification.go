package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"wahwah/lib"
)

func Notification (c *gin.Context){
	js := []string{
		"/asset/plugins/jquery-countto/jquery.countTo",
		"/asset/plugins/jquery-validation/jquery.validate",
		"/asset/plugins/raphael/raphael.min",
		"/asset/plugins/morrisjs/morris",
		"/asset/plugins/chartjs/Chart.bundle",
		"/asset/js/main/notification",

		//"asset/js/pages/forms/basic-form-elements",
	}
	css := []string{
		"/asset/plugins/waitme/waitMe",
		"/asset/plugins/bootstrap-select/css/bootstrap-select",
		"/asset/plugins/morrisjs/morris",

	}
	c.HTML(http.StatusOK,"notification/index",gin.H{
		"title" : "Kirim Noticitation",
		"js" : js,
		"css" : css,
	})
}

func NotificationAdd (c *gin.Context){
	var title = c.PostForm("title")
	var text = c.PostForm("text")

	var topicFirebase = "/topics/wahwahinfo"

	//var err = model.PushNotifNew(map[string]interface{}{
	//	"title":    title,
	//	"text":     text,
	//	"category": category,
	//	"topic":    topicFirebase,
	//})
	//if err != nil {
	//	lib.JSON(c, 500, err.Error(), gin.H{})
	//	return
	//}
	var fcm lib.SendFCM
	fcm.To = topicFirebase
	fcm.Notification.Title = title
	fcm.Notification.Text = text
	err := fcm.Send()
	if err != nil {
			lib.JSON(c, 500, err.Error(), gin.H{})
			return
		}
	lib.JSON(c, 200, "Berhasil tambah notifikasi", gin.H{})
	
}
