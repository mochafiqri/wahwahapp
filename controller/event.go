package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"wahwah/lib"
	"wahwah/model"
)

func EventPage (c *gin.Context){
	js := []string{
		"/asset/plugins/jquery-countto/jquery.countTo",
		"/asset/plugins/jquery-datatable/jquery.dataTables",
		"/asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap",
		"/asset/plugins/jquery-datatable/extensions/export/dataTables.buttons.min",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.flash.min",
		"/asset/plugins/jquery-datatable/extensions/export/jszip.min",
		"/asset/plugins/jquery-datatable/extensions/export/pdfmake.min",
		"/asset/plugins/jquery-datatable/extensions/export/vfs_fonts",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.html5.min",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.print.min",
		"/asset/plugins/jquery-validation/jquery.validate",
		"/asset/js/main/event",

		//"asset/js/pages/forms/basic-form-elements",
	}
	css := []string{
		"asset/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap",
		"/asset/plugins/waitme/waitMe",
		"/asset/plugins/bootstrap-select/css/bootstrap-select",
	}
	c.HTML(http.StatusOK,"event/index",gin.H{
		"title" : "Event Admin",
		"js" : js,
		"css" : css,
	})
}

func EventSettingPage (c *gin.Context){
	idEvent := c.Param("id_event")
	dataEvent,err := model.EventGetById(idEvent,"id,nama,waktu_mulai,waktu_berakhir,id_tempat,deskripsi,status,refundable")
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	var refundable = ""
	if dataEvent["refundable"].(string) == "1"{
		refundable = "checked"
	}
	band,err := model.BandGet("id,nama")
	if (dataEvent["waktu_mulai"].(string) == "0000-00-00 00:00:00"){
		dataEvent["waktu_mulai"] = nil
	}
	if (dataEvent["waktu_berakhir"].(string) == "0000-00-00 00:00:00"){
		dataEvent["waktu_berakhir"] = nil
	}

	js := []string{
		"/asset/plugins/momentjs/moment",
		"/asset/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker",
		"/asset/plugins/jquery-datatable/jquery.dataTables",
		"/asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap",
		"/asset/plugins/dropzone/dropzone",
		"/asset/js/main/eventDetail",

	}

	css := []string{
		"asset/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap",
		"/asset/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker",
		"/asset/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap",
		"/asset/plugins/waitme/waitMe",
		"/asset/plugins/dropzone/dropzone",
		"/asset/plugins/bootstrap-select/css/bootstrap-select",
	}
	venue,err := model.VenuneData()
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	kategori_tiket,err := model.TiketKategoriGet(idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	var status =  dataEvent["status"].(string)
	var progresss int = 25
	if status == "1" {
		progresss = 25
	}else if status == "2" {
		progresss = 50
	}else if status == "3" {
		progresss = 75
	}else if status == "4" {
		progresss = 90
	}else if status == "5" {
		progresss = 100
	}
	c.HTML(http.StatusOK,"event/setting",gin.H{
		"title" : "Event Setting",
		"venue" : venue,
		"js" : js,
		"css" : css,
		"event" : dataEvent,
		"progress" : progresss,
		"band" : band,
		"refundable" : refundable,
		"kategori_tiket" : kategori_tiket,
	})
}

func Eventdatatable (c *gin.Context){
	var data model.DatatablesSource
	err := c.Bind(&data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	result,err := model.Eventdatatable(data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	c.JSON(http.StatusOK,&result)
}

func EventUser(c *gin.Context){
	data,err := model.GetEventList()
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	var result = []map[string]interface{}{}
	for _,v := range data {
		dataFoto,_ := model.GetEventPosterMainByIdEvent(v["id"].(string))
		var poster = ""
		if dataFoto != nil {
			poster = dataFoto["foto"].(string)
		}
		result = append(result,map[string]interface{}{
			"poster" : poster,
			"id" : v["id"],
			"max_harga" : v["max_harga"],
			"min_harga" : v["min_harga"],
			"nama" : v["nama"],
			"nama_tempat" : v["nama_tempat"],
			"status" : v["status"],
			"tanggal_mulai" : v["tanggal_mulai"],
			"refundable" : v["refundable"],
		})
	}
	lib.JSON(c,http.StatusOK,"Event List",result)
	return
}

func EventDetailUser (c *gin.Context){
	idEvent := c.PostForm("id_event")
	data,err := model.EventDetail(idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Detail Event",data)
	return
}




func EventTambah (c *gin.Context){
	var ModelEvent = model.ModelEvent{}
	c.Bind(&ModelEvent)
	//var file,err = c.FormFile("banner")
	//if err != nil {
	//	lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
	//	return
	//}

	var insert = map[string]interface{}{
		"nama" : ModelEvent.Nama,
		"deskripsi" : ModelEvent.Deskripsi,
	}

	id,err := model.EventInsert(insert)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,"Terjadi error saat input event :"+err.Error(),gin.H{})
		return
	}
	fmt.Println(id)
	lib.JSON(c,http.StatusOK,"Berhasil Tambah Event",gin.H{
		"id_band" : id,
	})
	return

}

func EventGetById (c *gin.Context){
	id := c.PostForm("id")
	data,err := model.EventGetById(id,"id,nama,deskripsi")
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if len(data) == 0 || id == "" {
		lib.JSON(c,http.StatusNotFound,"Data Tidak di Temukan",gin.H{})
		return
	}

	lib.JSON(c,http.StatusOK,"Data Event",data)
	return

}

func EventBasicUpdate (c *gin.Context){
	var data = model.ModelEvent{}
	err := c.Bind(&data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	var update = map[string]interface{}{
		"nama" :data.Nama,
		"waktu_mulai" : data.WktMulai,
		"waktu_berakhir" : data.WktAkhir,
		"id_tempat" : data.IdTempat,
		"deskripsi" :data.Deskripsi,
	}
	fmt.Println(update)
	err = model.EventUpdate(data.Id,update)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Sukses Update Data Band",update)
	return
}

func SetPerformer(c *gin.Context){
	idEvent := c.PostForm("id_event")
	idBand := c.PostForm("id_band")

	data,err := model.GetEventBandByIdEvent(idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if len(data) == 0 {
		var insert = map[string]interface{}{
			"id_event" : idEvent,
			"id_band" : idBand,
		}
		_,err :=  model.InsertBand(insert)
		if err != nil {
			lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
			return
		}

	}else{
		var update = map[string]interface{}{
			"id_band" : idBand,
			"rundown" : 1,
		}
		err := model.EventBandUpdate(idEvent,update)
		if err != nil {
			lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
			return
		}
	}
	lib.JSON(c,http.StatusOK,"Sukses Set Performer",gin.H{})
	return
}

func EventSetStatus (c *gin.Context){
	var idEvent = c.PostForm("id_event")
	var status = c.PostForm("status")
	//
	//dataEvent,err := model.EventGetById(idEvent,"status")
	//if err != nil {
	//	lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
	//	return
	//}
	//
	//if dataEvent == nil || dataEvent["status"].(string) != "2" {
	//	lib.JSON(c,http.StatusInternalServerError,"Status event belum ditahap review",gin.H{})
	//	return
	//}

	var update = map[string]interface{}{
		"status" : status,
	}

	err := model.EventUpdate(idEvent,update)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Success To Next Step",gin.H{})
	return
}



func EventSelesai (c *gin.Context){
	var idEvent = c.PostForm("string")

	dataEvent,err := model.EventGetById(idEvent,"status")
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	if dataEvent == nil || dataEvent["status"].(string) != "2" {
		lib.JSON(c,http.StatusInternalServerError,"Status event belum ditahap review",gin.H{})
		return
	}

	var update = map[string]interface{}{
		"status" : 5,
	}

	err = model.EventUpdate(idEvent,update)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Sukses Set Selesai",gin.H{})
	return
}

func EventHomepage(c *gin.Context){
	event,err := model.GetEventHomepage()
	if err != nil {
		lib.JSON(c, http.StatusNotFound, err.Error(), gin.H{})
		return
	}
	var result = []map[string]interface{}{}
	for _,v := range event {
		dataFoto,_ := model.GetEventPosterMainByIdEvent(v["id"].(string))
		var poster = ""
		if dataFoto != nil {
			poster = dataFoto["foto"].(string)
		}
		result = append(result,map[string]interface{}{
			"poster" : poster,
			"id" : v["id"],
			"max_harga" : v["max_harga"],
			"min_harga" : v["min_harga"],
			"nama" : v["nama"],
			"nama_tempat" : v["nama_tempat"],
			"status" : v["status"],
			"tanggal_mulai" : v["tanggal_mulai"],
			"refundable" : v["refundable"],
		})
	}
	lib.JSON(c,http.StatusOK,"Band OK",result)
	return
}
