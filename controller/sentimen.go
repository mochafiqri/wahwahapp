package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"wahwah/lib"
	"wahwah/model"
)
func AnalyticPerformer (c *gin.Context){
	var band1 = ""
	var band2 = ""
	sentimen1 := (c.Query("sentimen1"))
	sentimen2 := (c.Query("sentimen2"))
	if sentimen1 != "" || sentimen2 != ""{

		databand1,err  := model.BandGetById(sentimen1,"nama,keyword,twitter")
		if err != nil {
			lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
			return
		}
		databand2,err  := model.BandGetById(sentimen2,"nama,keyword,twitter")

		if err != nil {
			lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
			return
		}
		if sentimen1 != ""{
			band1 = databand1["nama"].(string)
			band2 = databand2["nama"].(string)
		}
	}
	dataBand,err := model.BandGet("id,nama,keyword")
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	js := []string{
		"/asset/plugins/jquery-countto/jquery.countTo",
		"/asset/plugins/jquery-validation/jquery.validate",
		"/asset/plugins/raphael/raphael.min",
		"/asset/plugins/morrisjs/morris",
		"/asset/plugins/chartjs/Chart.bundle",
		"/asset/js/main/sentimen",

		//"asset/js/pages/forms/basic-form-elements",
	}
	css := []string{
		"/asset/plugins/waitme/waitMe",
		"/asset/plugins/bootstrap-select/css/bootstrap-select",
		"/asset/plugins/morrisjs/morris",

	}
	c.HTML(http.StatusOK,"ap/index",gin.H{
		"title" : "Analytic Performer",
		"js" : js,
		"css" : css,
		"sentimen1" : band1,
		"sentimen2" : band2,
		"sentimenId1" : sentimen1,
		"sentimenId2" : sentimen2,
		"dataBand" : dataBand,
	})
}

func Sentimen (c *gin.Context){
	var keyword = c.PostFormArray("keyword[]")
	fmt.Println(keyword);
	var result = []map[string]interface{}{}
	var result2 = map[string]interface{}{}
	var tweets = []string{}
	if len(keyword) == 0 || len(keyword) > 2 {
		lib.JSON(c,http.StatusBadRequest,"Jumlah keyword salah",gin.H{})
		return
	}
	var i = 1
		for _,v := range keyword {
			dataBand,err  := model.BandGetById(v,"nama,keyword,twitter")
			if err != nil {
				lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
				return
			}
			var keywordtweet string
			if dataBand["twitter"].(string) != ""{
				keywordtweet = dataBand["twitter"].(string)
			}else{
				keywordtweet = dataBand["nama"].(string)

			}
			tweet,err := model.TwitterGetTweet(keywordtweet)

			if err != nil {
				lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
				return
		}
		var postive float64
		var negative float64

		postive = 0
		negative = 0
		for _,t := range tweet{
			boolean := cekDuplikat(t,tweets)
			if (boolean){
				continue
			}
			t_id,err := model.Translate(t)
			if err != nil {
				lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
				return
			}
			tweets = append(tweets,t)
			tmpSentimen,err := model.SentimenNL(t_id)
			if err != nil {
				fmt.Println(err.Error())
				continue
			}
			if tmpSentimen == 1{
				postive = postive + 1
			}else{
				negative = negative + 1
			}

		}

		total := postive + negative
		istring := strconv.Itoa(i)
		result2["keyword"+istring] = v
		result2["sentimen_positive"+istring] = (postive/total)*100
		result2["sentimen_negative"+istring] = (negative/total)*100
		i = i + 1
		result = append(result,map[string]interface{}{
			"keyword" : v,
			"sentimen_positive" : postive,
			"sentimen_negative" : negative,
		})
	}

	lib.JSON(c,http.StatusOK,"Berhasil",result)
}

func cekDuplikat(tweet string, tweets []string) bool{
	var result bool
	result = false
	for _,v := range tweets{
		if (v == tweet){
			return true
		}
	}
	return result
}

func TestFloat(c *gin.Context){
	//var postive float64
	//var negative float64
	//
	//var total float64
	//
	//postive = 14
	//negative = 1
	//
	//total = postive + negative
	//fmt.Println((postive/total)*100)
	//fmt.Println((int(postive/total)*100))
	//fmt.Println((negative/total)*100)

	var tweets = []string{}

	for _,v := range tweets {
		if ("tes" == v){
			fmt.Println("oke")
		}
	}
	fmt.Println("oce")

}