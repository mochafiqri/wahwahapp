package controller

import (
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"wahwah/lib"
	"wahwah/model"
)

func MyTiket (c *gin.Context){
	var user = c.MustGet("jwt").(jwt.MapClaims)
	idUser := user["id"].(string)

	data,err := model.GetMyTiket(idUser)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"My Tiket ",data)
	return
}

func MyTiketDetail (c *gin.Context){
	idTiket := c.PostForm("id_tiket")
	dataOrder,err := model.GetOrderByIdTiket(idTiket)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if dataOrder == nil {
		lib.JSON(c,http.StatusNotFound,"Data Tidak Ditemukan",gin.H{})
		return
	}
	var idOrder = dataOrder["id_order"].(string)
	data,err := model.GetTiketByIdOrder(idOrder)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"My Tiket Detail ",data)
	return
}

