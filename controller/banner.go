package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strings"
	"wahwah/lib"
	"wahwah/model"
)

func BannerAdd (c *gin.Context){
	nama := c.PostForm("judul")
	file, err := c.FormFile("file")
	if file == nil && file.Filename == "" {
		lib.JSON(c, http.StatusInternalServerError, "Error File", gin.H{})
		return
	}
	tmp := file.Filename
	tmp1 := strings.Split(tmp, ".")
	tmp1[0] = tmp1[0]
	name := strings.Join(tmp1, ".")

	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	path := "./public/img/banner/"
	pathPoster := path+name
	link := "http://wahwah.my.id:8080/public/img/banner/"+name
	err = lib.Resize(file,1280,0,pathPoster)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	var insert = map[string]interface{}{
		"judul" : nama,
		"link" : link,
		"publish" : 1,
	}

	_,err = model.BannerAdd(insert)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}

	lib.JSON(c, http.StatusOK, "Berhasil Add Banner", gin.H{})
	return
}
