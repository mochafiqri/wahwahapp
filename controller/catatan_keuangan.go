package controller

import (
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"strconv"
	"wahwah/lib"
	"wahwah/model"
)

func CatatanUang (c *gin.Context){

	js := []string{
		"/asset/plugins/momentjs/moment",
		"/asset/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker",
		"/asset/plugins/jquery-datatable/jquery.dataTables",
		"/asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap",
		"/asset/js/main/catatan-keuangan",
	}

	css := []string{
		"asset/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap",
		"/asset/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker",
		"/asset/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap",
		"/asset/plugins/waitme/waitMe",
		"/asset/plugins/bootstrap-select/css/bootstrap-select",
	}

	idEvent := c.Param("id_event")
	var saldo int
	var debitInt int
	var kreditInt int
	event,err := model.EventGetById(idEvent,"id,nama")
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	data,err := model.GetCatatanKeuanganSaldo(idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if data == nil {
		saldo = 0
	}else{
		saldo,_ = strconv.Atoi(data["saldo"].(string))
	}
	dataKredit,err := model.GetCatataKeuanganKredit(idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if data == nil {
		saldo = 0
	}else{
		kreditInt,_ = strconv.Atoi(dataKredit["kredit"].(string))
	}
	dataDebit,err := model.GetCatataKeuanganDebit(idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if data == nil {
		saldo = 0
	}else{
		debitInt,_ = strconv.Atoi(dataDebit["debit"].(string))
	}


	debit,err := model.CatatanKeuanganKategoriGet("1")
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	kredit,err := model.CatatanKeuanganKategoriGet("2")
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	c.HTML(http.StatusOK,"catatan-keuangan/index",gin.H{
		"title" : "Catatan Keuangan",
		"js" : js,
		"css" : css,
		"saldo" : saldo,
		"event" : event,
		"debit" : debitInt,
		"kredit" : kreditInt,
		"kategori_debit" : debit,
		"kategori_kredit" : kredit,
	})

}

func CatatanKeuanganEvent(c *gin.Context){
	var idEvent = c.PostForm("id_event")
	var data model.DatatablesSource
	err := c.Bind(&data)

	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	result,err := model.CatataKeuanganEventDtb(data,idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	c.JSON(http.StatusOK,&result)
}

func AddCatatanKeuangan (c *gin.Context){
	var idEvent = c.PostForm("id_event")
	var idKategori = c.PostForm("id_kategori")
	var dataDebit = c.PostForm("debit")
	var dataKredit = c.PostForm("kredit")
	var ket = c.PostForm("keterangan")

	dataSaldo,err := model.GetCatatanKeuanganSaldo(idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	var saldo = 0
	if dataSaldo != nil {
		saldo,_ = strconv.Atoi(dataSaldo["saldo"].(string))
	}
	var kredit,_ = strconv.Atoi(dataKredit)
	var debit,_ = strconv.Atoi(dataDebit)
	saldo = saldo + debit - kredit

	var insert = map[string]interface{}{
		"id_event" : idEvent,
		"id_kategori" : idKategori,
		"kredit" : kredit,
		"debit" : debit,
		"saldo" : saldo,
		"ket" : ket,
	}
	_,err = model.CatatanKeuanganInsert(insert)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	lib.JSON(c,http.StatusOK,"Berhail Insert",gin.H{})
}


func AddConfirmByOrder (idOrder string) error{
	data, err := model.OrderById(idOrder, "id,id_event,total,kode_unik")
	if err != nil {
		return err
	}
	//Membuat keuangan penjualan tiket
	err = FunctionAddCatatanKeuangan(
		data["id_event"].(string),
		"1",
		data["total"].(string),
		"0",
		"Penjualan Tiket Order #"+idOrder,
	)

	if err != nil {
		return err
	}

	//Membuat keuangan penjualan tiket
	err = FunctionAddCatatanKeuangan(
		data["id_event"].(string),
		"2",
		data["kode_unik"].(string),
		"0",
		"Kode Unik #"+idOrder,
	)

return err
}

func FunctionAddCatatanKeuangan (idEvent,idKategori,dataDebit,dataKredit,ket string) error{

	dataSaldo,err := model.GetCatatanKeuanganSaldo(idEvent)
	if err != nil {
		return err
	}
	var saldo = 0
	if dataSaldo != nil {
		saldo,_ = strconv.Atoi(dataSaldo["saldo"].(string))
	}
	var kredit,_ = strconv.Atoi(dataKredit)
	var debit,_ = strconv.Atoi(dataDebit)
	saldo = saldo + debit - kredit

	var insert = map[string]interface{}{
		"id_event" : idEvent,
		"id_kategori" : idKategori,
		"kredit" : kredit,
		"debit" : debit,
		"saldo" : saldo,
		"ket" : ket,
	}
	_,err = model.CatatanKeuanganInsert(insert)
	if err != nil {
		return err
	}
	return err
}


func KeuanganExport (c *gin.Context){
	var idEvent = c.Query("id_event")

	data,err := model.GetCatatanKeuanganByIdEvent(idEvent)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	xlsx := excelize.NewFile()
	sheet1Name := "Sheet One"
	xlsx.SetSheetName(xlsx.GetSheetName(1), sheet1Name)
	xlsx.MergeCell(sheet1Name, "A1", "E1")
	xlsx.SetCellValue(sheet1Name, "A1", "CATATAN KEUANGAN")

	xlsx.SetCellValue(sheet1Name, "A3", "TANGGAL")
	xlsx.SetCellValue(sheet1Name, "B3", "KATEGORI")
	xlsx.SetCellValue(sheet1Name, "C3", "KREDIT")
	xlsx.SetCellValue(sheet1Name, "D3", "DEBIT")
	xlsx.SetCellValue(sheet1Name, "E3", "SALDO")
	xlsx.SetCellValue(sheet1Name, "F3", "KET")
	style, err := xlsx.NewStyle(`{
    "font": {go
        "bold": true,
        "size": 12
    }
	}`)

	err1 := xlsx.AutoFilter(sheet1Name, "A3", "E3", "")
	xlsx.SetCellStyle(sheet1Name, "A3", "E3", style)

	if err1 != nil {
		log.Fatal("ERROR", err1.Error())
	}

	for i, each := range data {
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("A%d", i+4), each["tanggal"])
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("B%d", i+4), each["kategori"])
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("C%d", i+4), each["kredit"])
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("D%d", i+4), each["debit"])
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("E%d", i+4), each["saldo"])
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("F%d", i+4), each["ket"])
	}
	c.Writer.Header().Set("Content-Type", "application/octet-stream")
	c.Writer.Header().Set("Content-Disposition", "attachment; filename="+"LaporanKeuangan.xlsx")
	c.Writer.Header().Set("Content-Transfer-Encoding", "binary")
	c.Writer.Header().Set("Expires", "0")
	err = xlsx.Write(c.Writer)

	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}


}


