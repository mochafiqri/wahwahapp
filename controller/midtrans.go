package controller

import (
	"strconv"
	"wahwah/model"
	"encoding/json"
	"fmt"
	"github.com/bandros/framework"
	"github.com/gin-gonic/gin"
	"github.com/veritrans/go-midtrans"
	"net/http"
	"wahwah/lib"
)

var midclient midtrans.Client
var coreGateway midtrans.CoreGateway
var snapGateway midtrans.SnapGateway

type Charge struct {
	PaymentType        string `json:"payment_type"`
	TransactionDetails struct {
		GrossAmount int    `json:"gross_amount"`
		OrderID     string `json:"order_id"`
	} `json:"transaction_details"`
	CustomerDetails struct {
		Email     string `json:"email"`
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
		Phone     string `json:"phone"`
	} `json:"customer_details"`
	BankTransfer struct {
		Bank string `json:"bank"`
	}
	//ItemDetails []struct {
	//	ID       string `json:"id"`
	//	Price    int    `json:"price"`
	//	Quantity int    `json:"quantity"`
	//	Name     string `json:"name"`
	//} `json:"item_details"`
	//
}


func ChargeDirect(c *gin.Context) {
	var api = framework.Api{}
	var charge = Charge{}
	charge.PaymentType = "bank_transfer"
	charge.BankTransfer.Bank = "permata"
	charge.TransactionDetails.OrderID = lib.GenerateOrderID()
	charge.TransactionDetails.GrossAmount = 200000

	api.Url = lib.APIMidtrans("v2/charge")
	api.Header = map[string]string{
		"Accept" : "application/json",
		"Content-Type" : "application/json",
	}
	api.JsonData(charge)

	//chargeResp, err := coreGateway.Charge(&midtrans.ChargeReq{
	//	PaymentType: midtrans.SourceBankTransfer,
	//	BankTransfer: &midtrans.BankTransferDetail{
	//		Bank:midtrans.BankBca},
	//
	//	TransactionDetails: midtrans.TransactionDetails{
	//		OrderID:  lib.GenerateOrderID(),
	//		GrossAmt: 200000,
	//	},
	//})
	api.Do("POST")
	type VaNumber struct{
		bank string   `json:"bank"`
		Va_Number string   `json:"va_number"`

	}
	var BCAVirtual struct {
		Status_Code string   `json:"status_code"`
		Status_Msg string   `json:"status_message"`
		Order_id string   `json:"order_id"`
		Transaction_status string   `json:"transaction_status"`
		Gross_amount string   `json:"gross_amount"`
		Va_Numbers VaNumber   `json:"va_numbers"`
	}

	err := api.Get(&BCAVirtual)
	fmt.Println(api.GetRaw())
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	//result, err := json.Marshal(chargeResp)
	//if err != nil {
	//	lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
	//	return
	//}

	//w.Header().Set("Content-Type", "application/json")
	//w.Write(result)
	c.JSON(http.StatusOK,BCAVirtual)
}

func ChargeMidtrans(c *gin.Context){
	midclient := midtrans.NewClient()
	midclient.ServerKey = "SB-Mid-server-2a1HmrgsJHHcxa3_tRLZ2yN0"
	midclient.ClientKey = "SB-Mid-client-yfkLmXC6miEmGrma"
	midclient.APIEnvType = midtrans.Sandbox

	coreGateway := midtrans.CoreGateway{
		Client: midclient,
	}

	chargeReq := &midtrans.ChargeReq{
		PaymentType: midtrans.SourceGopay,
		TransactionDetails: midtrans.TransactionDetails{
			OrderID: "220021200004",
			GrossAmt: 200000,
		},
		//MandiriBillBankTransferDetail: &midtrans.MandiriBillBankTransferDetail{
		//	BillInfo1: "asd",
		//	BillInfo2: "dsa",
		//},
		//BankTransfer: &midtrans.BankTransferDetail{
		//	Bank: midtrans.BankBca,
		//},
		Gopay:&midtrans.GopayDetail{
			EnableCallback: false,
			CallbackUrl:    "",
		},

		Items: &[]midtrans.ItemDetail{
			midtrans.ItemDetail{
				ID: "ITEM1",
				Price: 200000,
				Qty: 1,
				Name: "Someitem",
			},
		},
	}

	resp, err:= coreGateway.Charge(chargeReq)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	c.JSON(200,resp)

}

func PembayaranMidtrans(c *gin.Context){
	midclient := midtrans.NewClient()
	midclient.ServerKey = "SB-Mid-server-2a1HmrgsJHHcxa3_tRLZ2yN0"
	midclient.ClientKey = "SB-Mid-client-yfkLmXC6miEmGrma"
	midclient.APIEnvType = midtrans.Sandbox

	coreGateway := midtrans.CoreGateway{
		Client: midclient,
	}
	idOrder := c.PostForm("id_order")
	tipePembayaran := c.PostForm("id_pembayaran")
	dataPembayaran,err := model.TipePembayaranById(tipePembayaran)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if dataPembayaran == nil {
		lib.JSON(c,http.StatusInternalServerError,"Pembayaran Tidak ditemukan",gin.H{})
		return
	}

	var bank = dataPembayaran["nama_bank"].(string)
	var tipe = dataPembayaran["tipe_pembayaran"].(string)
	var va_number = ""
	var totalOrder = 0
	var tipeMidtrans midtrans.PaymentType

	var updateOrder = map[string]interface{}{
		"metode" : tipe,
		"bank" : bank,
		"tipe_pembayaran" : tipePembayaran,
	}
	if tipe == "transfer_bank" {
			tipeMidtrans = midtrans.SourceBankTransfer
		var bankMidtrans  midtrans.Bank
		if bank == "bca"{
			bankMidtrans = midtrans.BankBca
		} else if bank == "bni"{
			bankMidtrans = midtrans.BankBni
		} else if bank == "permata" {
			bankMidtrans = midtrans.BankPermata

		}

		dataOrder,err := model.OrderById(idOrder,"id,total total,kode_unik,qty,id_event")
		if err != nil {
			lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
			return
		}
		dataEvent,err :=model.EventGetById(dataOrder["id_event"].(string),"nama")
		if err != nil {
			lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
			return
		}
		//qty,_:=strconv .Atoi(dataOrder["qty"].(string))
		total,_ := strconv.Atoi(dataOrder["total"].(string))
		kode_unik,_ := strconv.Atoi(dataOrder["kode_unik"].(string))
		totalTiket := total + kode_unik
		totalTrf := totalTiket + 4000
		totalOrder = total + 4000

		//fmt.Println(totalTiket)
		//fmt.Println(totalTrf)
		//fmt.Println(totalOrder)
		chargeReq := &midtrans.ChargeReq{
			PaymentType: tipeMidtrans,
			TransactionDetails: midtrans.TransactionDetails{
				OrderID: idOrder,
				GrossAmt: int64(totalTrf),
			},
			BankTransfer: &midtrans.BankTransferDetail{
				Bank: bankMidtrans,
			},
			Items: &[]midtrans.ItemDetail{
				midtrans.ItemDetail{
					ID: dataEvent["nama"].(string),
					Price: int64(totalTiket),
					Qty: 1,
					Name: dataEvent["nama"].(string),
				},midtrans.ItemDetail{
					ID:"Tanggungan VA",
					Price: 4000,
					Qty: 1,
					Name: "Tanggungan VA",
				},
			},
		}

		resp, err:= coreGateway.Charge(chargeReq)
		if err != nil {
			lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
			return
		}
		fmt.Println(resp)
		if (resp.StatusCode != "200" || resp.StatusCode != "201" ){


		var va_numbers = resp.VANumbers
		va_number = va_numbers[0].VANumber
		updateOrder["virtual_account"] = va_number
		}else{
			lib.JSON(c, http.StatusInternalServerError,resp.StatusCode + " : "+resp.StatusMessage , idOrder)
		}

		updateOrder["total"] = totalOrder
	}else if tipe == "transfer_manual" {
		updateOrder["virtual_account"] = dataPembayaran["rekening"]
		updateOrder["status"] = 1
	}


	err = model.OrderUpdate(idOrder,updateOrder)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	lib.JSON(c, http.StatusOK, "Sukses Pembyaran", idOrder)

}

func ChargeWithMap(c *gin.Context) {

	var reqPayload = &midtrans.ChargeReqWithMap{}
	err := c.Bind(reqPayload)
	if err != nil {
		response := make(map[string]interface{})
		response["status_code"] = 400
		response["status_message"] = "please fill request payload, refer to https://api-docs.midtrans.com depend on payment method"

		lib.JSON(c,response["status_code"].(int),response["status_message"].(string),gin.H{})
		return
	}

	chargeResp, _ := coreGateway.ChargeWithMap(reqPayload)
	result, err := json.Marshal(chargeResp)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	c.JSON(200,result)
}

func NotificationMidtrans(c *gin.Context) {
	var reqPayload = &midtrans.ChargeReqWithMap{}
	err := c.Bind(reqPayload)
	if err != nil {
		response := make(map[string]interface{})
		response["status_code"] = 400
		response["status_message"] = "please fill request payload, refer to https://api-docs.midtrans.com/#receiving-notifications"

		lib.JSON(c,response["status_code"].(int),response["status_message"].(string),gin.H{})
		return
	}
	encode, _ := json.Marshal(reqPayload)

	resArray := make(map[string]string)
	err = json.Unmarshal(encode, &resArray)
	var status = 0
	if resArray["transaction_status"] == "pending"{
		status = 1
	}else if resArray["transaction_status"] == "settlement"{
		status = 3
		err = model.TiketUpdateConfirm(resArray["order_id"])
		if err != nil {
			lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
			return
		}

		//Membuat keuangan penjualan tiket
		_ = AddConfirmByOrder(resArray["order_id"])

	}
	var updateOrder = map[string]interface{}{
		"status_string" : resArray["transaction_status"],
		"status" : status,
	}
	fmt.Println(resArray["order_id"])
	err = model.OrderUpdate(resArray["order_id"],updateOrder)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	lib.JSON(c,http.StatusOK,"Notif Berhasil",gin.H{})
}

