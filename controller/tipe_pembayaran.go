package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"wahwah/lib"
	"wahwah/model"
)

func TipePembayaranList(c *gin.Context){
	data,err := model.ListTipePembayaran()
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Notif Berhasil",data)


}
