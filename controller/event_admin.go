package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
	"strings"
	"wahwah/lib"
	"wahwah/model"
)

func EventAdminAdd (c *gin.Context){
	var idEvent = c.PostForm("id_event")
	var idAdmin = c.PostForm("id_admin")
	var eventAdminKategori = c.PostForm("event_admin_kategori")
	data,err := model.EventGetById(idEvent,"id")
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	if (idEvent == "" || data == nil){
		lib.JSON(c,http.StatusBadRequest,"Event tidak tersedia",gin.H{})
		return
	}
	data , err = model.EventAdminByIdAdminIdEvent(idEvent,idAdmin)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	if data != nil {
		lib.JSON(c,http.StatusOK,"Petugas sudah terdaftar",gin.H{})
		return
	}

	var insert = map[string]interface{}{
		"id_admin" : idAdmin,
		"id_event" : idEvent,
		"event_admin_kategori" : eventAdminKategori,
	}

	_,err = model.EventAdminInsert(insert)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}

	lib.JSON(c,http.StatusOK,"Berhasil Menambahkan Petugas",gin.H{})

}


func LaporaEvent (c *gin.Context){
	idEvent := c.Param("id_event")
	js := []string{
		"/asset/js/dashboard",
		"/asset/plugins/jquery-countto/jquery.countTo",
		"/asset/plugins/raphael/raphael.min",
		"/asset/plugins/morrisjs/morris",
		"/asset/plugins/chartjs/Chart.bundle",
		"/asset/plugins/flot-charts/jquery.flot",
		"/asset/plugins/flot-charts/jquery.resize",
		"/asset/plugins/flot-charts/jquery.pie",
		"/asset/plugins/flot-charts/jquery.categories",
		"/asset/plugins/flot-charts/jquery.time",
		"/asset/plugins/jquery-sparkline/jquery.sparkline",
		"/asset/js/main/laporan_event",
	}
	css := []string{
		"/asset/plugins/morrisjs/morris",
		"/asset/plugins/node-waves/waves",
	}
	var saldo int
	var debitInt int
	var kreditInt int
	event,err := model.EventGetById(idEvent,"id,nama")
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	data,err := model.GetCatatanKeuanganSaldo(idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if data == nil {
		saldo = 0
	}else{
		saldo,_ = strconv.Atoi(data["saldo"].(string))
	}
	dataKredit,err := model.GetCatataKeuanganKredit(idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if data == nil {
		saldo = 0
	}else{
		kreditInt,_ = strconv.Atoi(dataKredit["kredit"].(string))
	}

	//Data Debit
	dataDebit,err := model.GetCatataKeuanganDebit(idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if data == nil {
		saldo = 0
	}else{
		debitInt,_ = strconv.Atoi(dataDebit["debit"].(string))
	}

	dataListDebit,err := model.GetDebitKeunanganDashboard(idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	dataListKredit,err := model.GetKreditKeunanganDashboard(idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}


	jmlOrder,err := model.OrderByIdEventJumlah(idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	var statusTiketTerjual = []string{
		"1",
		"2",
	}
	jmlTiketTerjual,err := model.TiketCountByStatus(idEvent,statusTiketTerjual)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	var statusTiketCheckin = []string{
		"2",
	}
	jmlTiketCheckin,err := model.TiketCountByStatus(idEvent,statusTiketCheckin)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	var statusTiketSisa = []string{
		"0",
	}
	jmlTiketSisa,err := model.TiketCountByStatus(idEvent,statusTiketSisa)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	rating,_ := model.GetAvgRatingByIdEvent(idEvent)

	review,_ := model.GetJmlReview(idEvent)

	listPenjualan_tiket,_ := model.TiketPenjualanByIdkategori(idEvent)
	var tmpArray = []string{}

	for _,v := range listPenjualan_tiket {
		tmpArray = append(tmpArray,v["jml"].(string))
	}
	chart_penjualan_tiket := strings.Join(tmpArray,",")
	fmt.Println(chart_penjualan_tiket)
	ListRating,_ := model.GetRatingPerKategoriByIdEvent(idEvent)
	c.HTML(http.StatusOK,"laporan-event/index",gin.H{
		"title" : "Catatan Keuangan",
		"js" : js, "css" : css,  "id_event" : idEvent,
		"saldo" : saldo,

		"event" : event,
		"debit" : debitInt,
		"listDebit" : dataListDebit,
		"kredit" : kreditInt,
		"listKredit" : dataListKredit,
		"jmlOrder" : jmlOrder,
		"jmlTiketTerjual" : jmlTiketTerjual,
		"jmlTiketCheckin" : jmlTiketCheckin,
		"jmlTiketSisa" : jmlTiketSisa,
		"rating" : rating["rating"].(string),
		"review" : review["jml"].(string),
		"listRating" : ListRating,
		"listPenjualan_tiket" : listPenjualan_tiket,
		"listpenjualanChart" : chart_penjualan_tiket,
	})

}

func SetRefundable (c *gin.Context){
	idEvent := c.PostForm("id_event")

	data,err := model.EventGetById(idEvent,"refundable")
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	var update = map[string]interface{}{}

	if data["refundable"].(string) == "0" {
		update["refundable"] = 1
	}else{
		update["refundable"] = 0
	}
	err = model.EventUpdate(idEvent,update)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	lib.JSON(c,http.StatusOK,"Berhasil Ganti Refundable ",gin.H{})

}