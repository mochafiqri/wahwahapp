package controller

import (
	"github.com/bandros/framework"
	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
	"wahwah/lib"
	"wahwah/model"
)

func TiketRefundPage(c *gin.Context) {
	js := []string{
		"/asset/plugins/jquery-countto/jquery.countTo",
		"/asset/plugins/jquery-datatable/jquery.dataTables",
		"/asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap",
		"/asset/plugins/jquery-datatable/extensions/export/dataTables.buttons.min",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.flash.min",
		"/asset/plugins/jquery-datatable/extensions/export/jszip.min",
		"/asset/plugins/jquery-datatable/extensions/export/pdfmake.min",
		"/asset/plugins/jquery-datatable/extensions/export/vfs_fonts",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.html5.min",
		"/asset/plugins/jquery-datatable/extensions/export/buttons.print.min",
		"/asset/plugins/jquery-validation/jquery.validate",
		"/asset/plugins/light-gallery/js/lightgallery",
		"/asset/js/main/tiket_refund",
	}
	css := []string{
		"asset/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap",
		"/asset/plugins/waitme/waitMe",
		"/asset/plugins/bootstrap-select/css/bootstrap-select",
		"/asset/plugins/light-gallery/css/lightgallery",
	}
	c.HTML(http.StatusOK, "tiket-refund/index", gin.H{
		"title": "Tiket Refund Admin",
		"js":    js,
		"css":   css,
	})
}

func MyTiketRefund (c *gin.Context){
	var user = c.MustGet("jwt").(jwt.MapClaims)
	idUser := user["id"].(string)

	idTiket := c.PostForm("id_tiket")
	bank := c.PostForm("bank")
	atas_nama := c.PostForm("atas_nama")
	norek := c.PostForm("no_rek")
	data,err := model.TiketByIdTiket(idTiket)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if data == nil || idTiket== "" {
		lib.JSON(c,http.StatusNotFound,"Tiket tidak ditemukan",gin.H{})
		return
	}
	if data["status"].(string) == "0"{
		lib.JSON(c,http.StatusInternalServerError,"Order Tiket Masih Belum Selesai",gin.H{})
		return
	}

	dataEvent,err := model.EventGetById(data["id_event"].(string),"id,refundable")
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	if dataEvent["refundable"].(string) != "1" {
		lib.JSON(c,http.StatusInternalServerError,"Event Tidak Refundable",gin.H{})
		return
	}

	dataRefund,err := model.GetRefundByIdTiketIdUser(idTiket,idUser)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	if dataRefund != nil {
		lib.JSON(c,http.StatusInternalServerError,"Tiket dalam proses di Refund",gin.H{})
		return
	}
	var update = map[string]interface{}{
		"status" : 3,
	}
	var db = framework.Database{}
	defer db.Close()

	db.Transaction()
	err = model.TiketUpdateDB(db,idTiket,update)
	if err != nil {
		db.Rollback()
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	now := time.Now().Format("2006-01-02 15:04:05")

	var insert = map[string]interface{}{
		"id_order" : data["id_order"],
		"id_tiket" : data["id_tiket"],
		"bank" : bank,
		"atas_nama" : atas_nama,
		"no_rekening" : norek,
		"tgl_request" : now,
		"id_user" : idUser,
		"jumlah" : data["harga"],
	}

	_,err = model.TiketRefundInsert(db,insert)
	if err != nil {
		db.Rollback()
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	db.Commit()
	lib.JSON(c,http.StatusOK,"Request Refund Berhasil ",gin.H{})
	return

}

func MyTiketRefundList (c *gin.Context) {
	var user = c.MustGet("jwt").(jwt.MapClaims)
	idUser := user["id"].(string)

	data,err := model.GetRefundByIdUser(idUser)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	lib.JSON(c,http.StatusOK," Data Refund List ",data)
	return
}

func RefundPendingDataTable (c *gin.Context){
	var data model.DatatablesSource
	err := c.Bind(&data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	var status = []string{"0"}

	result,err := model.TiketRefunddatatable(data,status)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	c.JSON(http.StatusOK,&result)
}

func RefundDataTable (c *gin.Context){
	var data model.DatatablesSource
	err := c.Bind(&data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	var status = []string{"1","2"}
	result,err := model.TiketRefunddatatable(data,status)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	c.JSON(http.StatusOK,&result)
}

func RefundCancel(c *gin.Context){
	idRefund := c.PostForm("id_refund")

	data,err := model.GetDataRefundByIdRefund(idRefund)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if data["status"].(string) == "2"{
		lib.JSON(c,http.StatusInternalServerError,"Data refund sudah tercancel ",gin.H{})
		return
	}

	var updateDataRefund = map[string]interface{}{
		"status" : 2,
	}
	var updateDataTiket = map[string]interface{}{
		"status" : 1,
	}
	var db = framework.Database{}
	defer db.Close()

	db.Transaction()

	err = model.UpdateDataRefundDb(db,idRefund,updateDataRefund)
	if err != nil {
		db.Rollback()
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	err = model.TiketUpdateDB(db,data["id_tiket"].(string),updateDataTiket)
	if err != nil {
		db.Rollback()
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	db.Commit()



	lib.JSON(c,http.StatusOK,"Cancel Refund Berhasil ",gin.H{})
	return
}

func RefundConfirm(c *gin.Context){
	idRefund := c.PostForm("id_refund")

	data,err := model.GetDataRefundByIdRefund(idRefund)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if data["status"].(string) == "1"{
		lib.JSON(c,http.StatusInternalServerError,"Data refund sudah terkonfirm ",gin.H{})
		return
	}
	var db = framework.Database{}
	defer db.Close()
	var updateTiket = map[string]interface{}{
		"id_order" : "",
		"status" : 0,
	}
	err = model.TiketUpdateDB(db,data["id_tiket"].(string),updateTiket)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	var now = time.Now()
	var tanggal = now.Format("2006-01-02 15:04:05")
	var updateDataRefund = map[string]interface{}{
		"status" : 1,
		"tgl_refund" : tanggal,
	}

	err = model.UpdateDataRefundDb(db,idRefund,updateDataRefund)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	db.Commit()
	lib.JSON(c,http.StatusOK,"Cancel Refund Berhasil ",gin.H{})
	return
}
