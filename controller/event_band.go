package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"wahwah/lib"
	"wahwah/model"
)

func EevntBanddatatable (c *gin.Context){
	var data model.DatatablesSource
	err := c.Bind(&data)
	idEvent := c.PostForm("id_event")
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	result,err := model.EventBanddatatable(data,idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	c.JSON(http.StatusOK,&result)
}