package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"wahwah/lib"
	"wahwah/model"
)

func EventPoster (c *gin.Context){

	js := []string{
		"/asset/plugins/momentjs/moment",
		"/asset/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker",
		"/asset/plugins/jquery-datatable/jquery.dataTables",
		"/asset/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap",
		"/asset/plugins/dropzone/dropzone",
		"/asset/js/main/poster",
	}

	css := []string{
		"asset/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap",
		"/asset/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker",
		"/asset/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap",
		"/asset/plugins/waitme/waitMe",
		"/asset/plugins/bootstrap-select/css/bootstrap-select",
		"/asset/plugins/dropzone/dropzone",

	}

	idEvent := c.Param("id_event")

	event,err := model.EventGetById(idEvent,"id,nama")
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if event == nil {
		c.HTML(http.StatusOK,"template/404",gin.H{
			"title" : "Halaman Tidak Ditemukan",
		})
	}
	var poster = "/asset/images/profile-post-image.jpg"
	dataPoster,err := model.GetEventPosterMainByIdEvent(idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	if dataPoster != nil {
		poster = dataPoster["foto"].(string)
	}

	c.HTML(http.StatusOK,"event/poster",gin.H{
		"title" : "Add Poster",
		"js" : js,
		"css" : css,
		"event" : event,
		"poster_main" : poster,
	})

}

func AddPoster (c *gin.Context){
	idEvent := c.PostForm("id_event")
	file, err := c.FormFile("file")
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError,"Error File =>"+ err.Error(), gin.H{})
		return
	}
	var link = ""
	var pathPoster = ""
	if file != nil  && file.Filename != "" {

		path := "./public/img/event_poster/"
		pathPoster = path+file.Filename
		link = "http://wahwah.my.id:8080/public/img/event_poster/"+file.Filename

		err := c.SaveUploadedFile(file, pathPoster)
		if err != nil {
			lib.JSON(c, http.StatusInternalServerError, "Error Upload => "+err.Error(), gin.H{})
			return
		}
	}else{
		lib.JSON(c, http.StatusInternalServerError, "File Error", gin.H{})
		return
	}
	data,err := model.GetEventPosterMainByIdEvent(idEvent)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}

	if data != nil {
		var update = map[string]interface{}{
			"main" : 0,
		}

		err = model.UpdateEventPoster(update,data["id"].(string))
		if err != nil {
			lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
			return
		}
	}
	var insert = map[string]interface{}{
		"id_event" : idEvent,
		"foto" : link,
		"publish" : 1,
		"main" : 1,
	}
	_,err = model.InserEventPoster(insert)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	c.Redirect(http.StatusFound,"/event-poster/"+idEvent)
}


