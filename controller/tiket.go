package controller

import (
	"fmt"
	"github.com/360EntSecGroup-Skylar/excelize"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"strconv"
	"time"
	"wahwah/lib"
	"wahwah/model"
)

func TiketAdd (c *gin.Context){
	var data model.TiketModel
	err := c.Bind(&data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}



	var qtyS = c.PostForm("qty")
	qty,err := strconv.Atoi(qtyS)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	for i:=1;i<=qty;i++{
		_,err := model.TiketAdd(data)
		if err != nil {
			lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
			return
		}
	}
	//dataEvent,_ := model.EventGetById(data.IdEvent,"status")
	//if dataEvent["status"].(string) == "1" {
	//	var update = map[string]interface{}{
	//		"status" : 2,
	//	}
	//	err = model.EventUpdate(data.IdEvent,update)
	//	if err != nil {
	//		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
	//		return
	//	}
	//}

	lib.JSON(c,http.StatusOK,"Sukses Membuat Tiket ",gin.H{})
	return
}

func TiketKategoriList(c *gin.Context){
	idEvent := c.PostForm("id_event")
	var data model.DatatablesSource
	err := c.Bind(&data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	result,err := model.TiketKategoridatatable(data,idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	c.JSON(http.StatusOK,&result)
}

func TiketKategoriAdd (c *gin.Context) {
	idEvent := c.PostForm("id_event")
	namaKategori := c.PostForm("nama_kategori")
	harga := c.PostForm("harga")

	var insert = map[string]interface{}{
		"id_event" : idEvent,
		"nama" : namaKategori,
		"harga" : harga,
	}

	_,err := model.TiketKategoriAdd(insert)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	lib.JSON(c,http.StatusOK,"Sukses Membuat Tiket Kategori",insert)
	return
}
func TiketKategoriGet (c *gin.Context){
	idEvent := c.PostForm("id_event")
	data,err := model.TiketKategoriGet(idEvent)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Tiket Kategori",data)
	return
}

func SetTiketKategoriSold (c *gin.Context){
	idTiketKategori := c.PostForm("id_tiket_kategori")
	fmt.Println(idTiketKategori)
	data,err := model.TiketKategoriById(idTiketKategori)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if data == nil {
		lib.JSON(c,http.StatusNotFound,"Data tidak ditemukan",gin.H{})
		return
	}

	var update = map[string]interface{}{}
	if data["status"].(string) == "1"{
		update["status"] = 2
	}else{
		update["status"] = 1
	}
	err = model.TiketKategoriUpdate(update,idTiketKategori)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Sukses Set tiket ",gin.H{})
	return
}

func TiketSold (c *gin.Context){
	var data model.DatatablesSource
	err := c.Bind(&data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	idEvent := c.PostForm("id_event")
	if idEvent == ""{
		lib.JSON(c,http.StatusNotFound,"Id Event Harus Diisi",gin.H{})
		return
	}
	result,err := model.TiketSoldByidEventDatatable(idEvent,data)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	c.JSON(http.StatusOK,&result)
}

func TiketGetById (c *gin.Context){
	var idTiket = c.PostForm("id_tiket")
	data,err := model.TiketByIdTiket(idTiket)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if data == nil || idTiket == ""{
		lib.JSON(c,http.StatusNotFound,"Data Tiket Tidak Ditemukan",gin.H{})
		return
	}

	lib.JSON(c,http.StatusOK,"Tiket Detail",data)
}

func TiketCheckin(c *gin.Context){
	var idTiket = c.PostForm("id_tiket")
	var now = time.Now()
	var tanggal = now.Format("2006-01-02 15:04:05")
	data,err := model.TiketByIdTiket(idTiket)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if data == nil {
		lib.JSON(c,http.StatusNotFound,"Tiket Tidak Diteemukan",gin.H{})
		return
	}
	if data["status"].(string) == "0"{
		lib.JSON(c,http.StatusInternalServerError,"Order Tiket Masih Belum Selesai",gin.H{})
		return
	}
	if data["status"].(string) == "2"{
		lib.JSON(c,http.StatusInternalServerError,"Tiket Sudah Melakaukan Check-in",gin.H{})
		return
	}
	var update = map[string]interface{}{
		"checkin" : tanggal,
		"status" : 2,
	}
	err = model.TiketUpdate(idTiket,update)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	dataEvent,_ := model.EventGetById(data["id_event"].(string),"status")
	if dataEvent["status"].(string) == "2" {
		var update = map[string]interface{}{
			"status" : 3,
		}
		err = model.EventUpdate(data["id_event"].(string),update)
		if err != nil {
			lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
			return
		}
	}
	dataOrder,err := model.GetOrderByIdTiket(idTiket)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	if dataOrder == nil {
		lib.JSON(c,http.StatusNotFound,"Data Tidak Ditemukan",gin.H{})
		return
	}
	var idOrder = dataOrder["id_order"].(string)
	result,err := model.GetTiketByIdOrder(idOrder)
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}

	lib.JSON(c,http.StatusOK,"Verifikasi Berhasil",result)
}

func LastCheckin(c *gin.Context){
	data,err := model.TiketLastCheckin()
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Last Checkin",data)

}

func TiketHomepage(c *gin.Context){
	data,err := model.TiketHomepage()
	if err != nil {
		lib.JSON(c,http.StatusInternalServerError,err.Error(),gin.H{})
		return
	}
	lib.JSON(c,http.StatusOK,"Last Checkin",data)
}

func TiketExport (c *gin.Context){
	var idEvent = c.Query("id_event")

	data,err := model.TiketByEvent(idEvent)
	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}
	xlsx := excelize.NewFile()
	sheet1Name := "Sheet One"
	xlsx.SetSheetName(xlsx.GetSheetName(1), sheet1Name)
	xlsx.MergeCell(sheet1Name, "A1", "E1")
	xlsx.SetCellValue(sheet1Name, "A1", "Tiket")

	xlsx.SetCellValue(sheet1Name, "A3", "ID TIKET")
	xlsx.SetCellValue(sheet1Name, "B3", "ID ORDER")
	xlsx.SetCellValue(sheet1Name, "C3", "Kategori Tiket")
	xlsx.SetCellValue(sheet1Name, "D3", "NAMA")
	xlsx.SetCellValue(sheet1Name, "E3", "STATUS TIKET")
	xlsx.SetCellValue(sheet1Name, "F3", "TANGGAL CHECKIN")
	style, err := xlsx.NewStyle(`{
    "font": {go
        "bold": true,
        "size": 12
    }
	}`)

	err1 := xlsx.AutoFilter(sheet1Name, "A3", "E3", "")
	xlsx.SetCellStyle(sheet1Name, "A3", "E3", style)

	if err1 != nil {
		log.Fatal("ERROR", err1.Error())
	}

	for i, each := range data {
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("A%d", i+4), each["id"])
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("B%d", i+4), each["id_order"])
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("C%d", i+4), each["nama_kategori"])
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("D%d", i+4), each["nama_pembeli"])
		var status string
		if each["status"].(string) == "2"{
			status = "Sudah Checkin"
		}else if each["status"].(string) == "1"{
			status = "Belum Checkin"
		}
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("E%d", i+4), status)
		xlsx.SetCellValue(sheet1Name, fmt.Sprintf("F%d", i+4), each["checkin"])
	}
	c.Writer.Header().Set("Content-Type", "application/octet-stream")
	c.Writer.Header().Set("Content-Disposition", "attachment; filename="+"LaporanTiket.xlsx")
	c.Writer.Header().Set("Content-Transfer-Encoding", "binary")
	c.Writer.Header().Set("Expires", "0")
	err = xlsx.Write(c.Writer)

	if err != nil {
		lib.JSON(c, http.StatusInternalServerError, err.Error(), gin.H{})
		return
	}


}
